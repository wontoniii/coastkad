import sys
import os
import getopt
from operator import itemgetter
from alto import AltoClient as Alto

#Tempo di download totale
RTIME = '1'
#Tempo download primo chunk
FTIME = '2'
#Numero di contenuti all'istante
NCONTS = '3'
#Scelto HTTP all'istante
HTTPC = '4'
#Scelto P2P
P2PC = '5'
#Scelta CDN
CDNT = '6'
#Tempo per download via P2P
P2PT = '7'
#Download starte at...
DT = '8'
#Tempo per download via CDN del primo chunk
CDNFCT = '9'
#Tempo per download via P2P del primo chunk
P2PFCT = '10'
#Tempo di inizio simulazione
INITT = '11'
#Tempo di fine simulazione
FL = '12'
#Tempo per download via Cache del primo chunk
CACHEFCT = '13'
#Tempo per download via Cache
CACHEC = '14'
#Chunk downloaded from cdn
CDNC = '15'
#Chunk downloaded from cep
CEPC = '16'
#Time to start the reproduction of the video
VINIT = '17'
#Timeout error downloading from CDN
CCTO = '18'
#Timeout error downloading from CEP node
CETO = '19'
#404 error downloading from CDN
CC404 = '20'
#404 error downloading from CEP
CE404 = '21'

alt = Alto("NodesCoordinates")

def printHeader(f):
	print >>f,"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
	print >>f,"<gexf xmlns=\"http://www.gexf.net/1.2draft\""
	print >>f,"\txmlns:viz=\"http://www.gexf.net/1.2draft/viz\""
	print >>f,"\txmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
	print >>f,"\txsi:schemaLocation=\"http://www.gexf.net/1.2draft"
	print >>f,"\thttp://www.gexf.net/1.2draft/gexf.xsd\""
	print >>f,"\tversion=\"1.2\">"
	
def openGraph(f):
	print >>f, "\t<graph mode=\"dynamic\" defaultedgetype=\"directed\" start=\"0\" end=\"36001.0\">"
	
def addAttribute(f,attr,title,typ):
	print >>f, "\t\t<attributes class=\"node\" mode=\"dynamic\">"
	print >>f, "\t\t\t<attribute id=\""+ attr + "\" title=\""+title+"\" type=\""+typ+"\">"
	#print >>f, "\t\t\t\t<default>0</default>"
	print >>f, "\t\t\t</attribute>"
	print >>f, "\t\t</attributes>"
	print >>f, "\t\t<attributes class=\"node\" mode=\"static\">"
	print >>f, "\t\t\t<attribute id=\"latitude\" title=\"latitude\" type=\"double\">"
	#print >>f, "\t\t\t\t<default>0</default>"
	print >>f, "\t\t\t</attribute>"
	print >>f, "\t\t\t<attribute id=\"longitude\" title=\"longitude\" type=\"double\">"
	#print >>f, "\t\t\t\t<default>0</default>"
	print >>f, "\t\t\t</attribute>"
	print >>f, "\t\t</attributes>"
	
def openNodes(f):
	print >>f, "\t\t<nodes>"

def addNode(f,node,label,r,g,b,attr, attrs, lat, lon):
	print >>f, "\t\t\t<node id=\""+node+"\" label=\""+label+"\" start=\"0\">"
	print >>f, "\t\t\t\t<viz:color r=\""+r+"\" g=\""+g+"\" b=\""+b+"\"/>"
	print >>f, "\t\t\t\t<viz:position x=\""+lat+"\" y=\""+lon+"\"></viz:position>"
	print >>f, "\t\t\t\t<attvalues>"
	print >>f, "\t\t\t\t\t<attvalue for=\"latitude\" value=\""+lat+"\" />"
	print >>f, "\t\t\t\t\t<attvalue for=\"longitude\" value=\""+lon+"\" />"
	for a in attrs:
		print >>f, "\t\t\t\t\t<attvalue for=\""+attr+"\" start=\""+a[1]+"\" endopen=\""+a[2]+"\" value=\""+a[0]+"\" />"
	print >>f, "\t\t\t\t</attvalues>"
	print >>f, "\t\t\t</node>"

def endNodes(f):
	print >>f, "\t\t</nodes>"

def openEdges(f):
	print >>f, "\t\t<edges>"
	
def addEdge(f,n1,n2):
	print >>f, "\t\t\t<edge source=\""+n1+"\" target=\""+n2+"\" start=\"0\"/>"
	
def endEdges(f):
	print >>f, "\t\t</edges>"
	
def closeUp(f):
	print >>f, "\t</graph>"
	print >>f, "</gexf>"

def getCepsfiles():
	f = open('PLNodesExp','r')
	nodes = []
	for line in f:
		n = line.split(',')
		nodes.append(n[0].rsplit()[0])
	f.close()
	return nodes

def getStats(nodes):
	stats = {}
	for node in nodes:
		try:
			f = open('stats/'+node+'.stats','r')
		except Exception:
			print node, "not found"
			continue
		name = node
		stats[name] = []
		for line in f:
			l = line.split(',')
			n = int(l[0])-1
			if n == 14 or n == 15:
				try:
					stats[name].append(float(l[1]))
				except Exception:
					continue
		f.close()
	return stats
	
def genNode(f, node, times, interval, genInit):
	g = '0'
	b = '0'
	if node in ["a1","a2","a3","a4","a5","a6","a7","a8"]:
		r='255'
	else:
		r='0'
	attrs=[]
	stab = 0.5
	prev1 = -1
	prev2 = -1
	cont = 0
	ndone = True
	init = genInit
	end = genInit + interval
	for t in times:
		if ndone and t > 18000:
			ndone = False
			interval = interval/2
			stab = 1
		if t > end:
			if prev1 > -1 and prev2 >= 0:
				attrs.append((str(((cont+prev1+prev2)/3)*stab),str(init - genInit),str(end - genInit)))
			init += interval
			end += interval
			prev2 = prev1
			prev1 = cont
			cont = 1
		else:
			cont+=1
	lat,lon = alt.getCoords(node)
	addNode(f,node,node,r,g,b,"weight",attrs, str(lat*10), str(lon*10))
	
def genEdges(f, stats, distance):
	openEdges(f)
	for key in stats:
		if key not in ["a1","a2","a3","a4","a5","a6","a7","a8"]:
			ret = alt.analyze(key, stats.keys(), distance)
			for name in ret:
				addEdge(f, key, name[0])
	endEdges(f)

def genGraph(stats, interval, distance, genInit):
	f = open("coast.gexf",'w')
	printHeader(f)
	openGraph(f)
	addAttribute(f,"weight","Weight","integer")
	openNodes(f)
	for key in stats:
		genNode(f,key,stats[key], interval, genInit)
	endNodes(f)
	genEdges(f, stats, distance)
	closeUp(f)
	f.close()
		
def usage():
	print "USAGE:\n"
	print "\t-d | --distance = max distances between nodes"
	print "\t-i | --interval = interval size for sampling"
	
	print "\t-h | -help = display this\n"
		
if __name__ == "__main__":

	opts = None
	args = None	
	try:
		opts, args = getopt.getopt(sys.argv[1:], "i:d:h", ["help", "distance=", "interval="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
	interval = 0
	distance = 0
	
	for o, a in opts:
		if o in ("-i", "--interval"):
			interval = float(a)
			function = a.split(",")
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-d", "--distance"):
			distance = float(a)
		else:
			assert False, "unhandled option"
	if interval == 0 or distance == 0:
		print "You need to set an interval and a distance\n"
		usage()
		sys.exit()
	nodes = getCepsfiles()
	stats = getStats(nodes)
	#print stats.keys()
	#print stats
	init = stats["a1"][0]
	for key in stats:
		print key
		if key[0] < init:
			init = key[0]
	genGraph(stats, interval, distance, init)
	sys.exit()
