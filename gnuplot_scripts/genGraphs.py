import sys
import os
import getopt
from operator import itemgetter

#Tempo di download totale
RTIME = '1'
#Tempo download primo chunk
FTIME = '2'
#Numero di contenuti all'istante
NCONTS = '3'
#Scelto HTTP all'istante
HTTPC = '4'
#Scelto P2P
P2PC = '5'
#Scelta CDN
CDNT = '6'
#Tempo per download via P2P
P2PT = '7'
#Download starte at...
DT = '8'
#Tempo per download via CDN del primo chunk
CDNFCT = '9'
#Tempo per download via P2P del primo chunk
P2PFCT = '10'
#Tempo di inizio simulazione
INITT = '11'
#Tempo di fine simulazione
FL = '12'
#Tempo per download via Cache del primo chunk
CACHEFCT = '13'
#Tempo per download via Cache
CACHEC = '14'
#Chunk downloaded from cdn
CDNC = '15'
#Chunk downloaded from cep
CEPC = '16'
#Time to start the reproduction of the video
VINIT = '17'
#Timeout error downloading from CDN
CCTO = '18'
#Timeout error downloading from CEP node
CETO = '19'
#404 error downloading from CDN
CC404 = '20'
#404 error downloading from CEP
CE404 = '21'


def getCepsfiles():
	f = open('utils/PLNodesExp','r')
	nodes = []
	for line in f:
		n = line.split(',')
		nodes.append(n[0].rsplit()[0])
	f.close()
	#nodes.append("a1")
	#nodes.append("a2")
	#nodes.append("a3")
	#nodes.append("a4")
	#nodes.append("a5")
	#nodes.append("a6")
	#nodes.append("a7")
	#nodes.append("a8")
	return nodes
	
def getStats(nodes):
	stats = []
	for i in range(21):
		stats.append([])
	for node in nodes:
		try:
			f = open('stats/'+node+'.stats','r')
		except Exception:
			continue
		for line in f:
			l = line.split(',')
			#Attivare queste linee per non analizzare i dati dei singoli chunk
			#if l[0] == 14 or l[0] == 15:
			#	continue
			if len(l) == 2:
				try:
					stats[int(l[0])-1].append(float(l[1]))
				except Exception:
					print "ERROR"
			elif len(l) == 3:
				try:
					stats[int(l[0])-1].append((float(l[1]),float(l[2])))
				except Exception:
					print "ERROR" 
		f.close()
	for s in stats:
		print len(s)
	return stats
	
def nullFunc(stats):
	return
	
	
def RTIMEGraph(append, stats, size, unit, end, init):
	"""
		Structure raw datas for creating the 
	"""
	f = open(append+'RTIMEGraph','w')
	stats = sorted(stats, key=itemgetter(1))
	absTime = init
	iTime = init
	fTime = init + size
	i = 0.0
	window = []
	temp = []
	while iTime < end:
		for t in window:
			if t[1] > iTime:
				temp.append(t)
		window = temp
		for s in stats:
			if s[1] > fTime:
				break
			window.append(stats.pop(0))
		for t in window:
			i += t[0]
		try:
			i = i/len(window)
			#Stampo sul file le cose per il grafico
			f.write(str(fTime-absTime)+' '+str(i)+'\n')
		except Exception:
			#print "ECCEZIONE",`sys.exc_info()`
			pass
		#Resetto le var
		iTime += unit
		fTime += unit
		temp = []
		i = 0
	f.close()
			
	
def RTIMEGraph2(append, stats, size):
	f2 = open(append+'RTIMEGraph2cdf','w')
	times = []
	for s in stats:
		times.append(s[0])
	n = size/60
	probs = []
	for i in range(60):
		probs.append(0)
	for t in times:
		for i in range(60):
			if t > i*n and t <= i*n+n:
				probs[i] += 1
				break
			if i == 59:
				probs[59] += 1
	k = 0.0
	for i in range(60):
		k += float(probs[i])/float(len(times))
		f2.write(str(i*n+n/2)+' '+str(k)+'\n')
	f2.close()
	
def TYPEGraphP2P(append, cdn, p2p, cache, size, unit, end, init):
	f1 = open(append+'TYPEGraphP2P1','w')
	f2 = open(append+'TYPEGraphP2P2','w')
	f3 = open(append+'TYPEGraphP2P3','w')
	cdn.sort()
	p2p.sort()
	cache.sort()
	absTime = init
	iTime = init
	fTime = init + size
	i1 = 0
	window1 = []
	temp1 = []
	i2 = 0
	window2 = []
	temp2 = []
	i3 = 0
	window3 = []
	temp3 = []
	while iTime < end:
		for t in window1:
			if t > iTime:
				temp1.append(t)
		window1 = temp1
		for s in cdn:
			if s > fTime:
				break
			window1.append(cdn.pop(0))
		for t in window1:
			i1 += 1
		
		for t in window2:
			if t > iTime:
				temp2.append(t)
		window2 = temp2
		for s in p2p:
			if s > fTime:
				break
			window2.append(p2p.pop(0))
		for t in window2:
			i2 += 1
		
		for t in window3:
			if t > iTime:
				temp3.append(t)
		window3 = temp3
		for s in cache:
			if s > fTime:
				break
			window3.append(cache.pop(0))
		for t in window3:
			i3 += 1
		
		try:
			tot = i1+i2+i3
			i1 = float(i1)/float(tot)
			i2 = float(i2)/float(tot)
			i3 = float(i3)/float(tot)
			#Stampo sul file le cose per il grafico
			f1.write(str(fTime-absTime)+' '+str(i1)+'\n')
			f2.write(str(fTime-absTime)+' '+str(i2)+'\n')
			f3.write(str(fTime-absTime)+' '+str(i3)+'\n')
		except Exception:
			#print "ECCEZIONE",`sys.exc_info()`
			pass
		#Resetto le var
		iTime += unit
		fTime += unit
		temp1 = []
		i1 = 0
		temp2 = []
		i2 = 0
		temp3 = []
		i3 = 0
	f1.close()
	f2.close()
	f3.close()
	
def TYPEGraphNoP2P(append, cdn, cache, size, unit, end, init):
	f1 = open(append+'TYPEGraphNoP2P1','w')
	f2 = open(append+'TYPEGraphNoP2P2','w')
	cdn.sort()
	cache.sort()
	absTime = init
	iTime = init
	fTime = init + size
	i1 = 0
	window1 = []
	temp1 = []
	i3 = 0
	window3 = []
	temp3 = []
	while iTime < end:
		for t in window1:
			if t > iTime:
				temp1.append(t)
		window1 = temp1
		for s in cdn:
			if s > fTime:
				break
			window1.append(cdn.pop(0))
		for t in window1:
			i1 += 1
		
		for t in window3:
			if t > iTime:
				temp3.append(t)
		window3 = temp3
		for s in cache:
			if s > fTime:
				break
			window3.append(cache.pop(0))
		for t in window3:
			i3 += 1
		
		try:
			tot = i1+i3
			i1 = float(i1)/float(tot)
			i3 = float(i3)/float(tot)
			#Stampo sul file le cose per il grafico
			f1.write(str(fTime-absTime)+' '+str(i1)+'\n')
			f2.write(str(fTime-absTime)+' '+str(i3)+'\n')
		except Exception:
			print "ERROR", `sys.exc_info()`
			pass
		#Resetto le var
		iTime += unit
		fTime += unit
		temp1 = []
		i1 = 0
		temp3 = []
		i3 = 0
	f1.close()
	f2.close()
	
def TYPEGraphP2PChunk(append, cdn, p2p, size, unit, end, init):
	"""
		Generate the raw data used to create a graph showing the distribution of the requests of the chunks beetween cdn, p2p
	"""
	f1 = open(append+'TYPEGraphP2PChunk1','w')
	f2 = open(append+'TYPEGraphP2PChunk2','w')
	print "Genero i dati per i grafici."
	print "Tempo iniziale:",init
	print "Tempo finale:", end
	print "Dimensione finestra:", size
	print "Scorrimento finestra:", unit
	cdn.sort()
	p2p.sort()
	absTime = init
	iTime = init
	fTime = init + size
	i1 = 0
	window1 = []
	temp1 = []
	i2 = 0
	window2 = []
	temp2 = []
	while iTime < end:
		print iTime
		for t in window1:
			if t > iTime:
				temp1.append(t)
		window1 = temp1
		for s in cdn:
			if s > fTime:
				break
			window1.append(cdn.pop(0))
		for t in window1:
			i1 += 1
		
		for t in window2:
			if t > iTime:
				temp2.append(t)
		window2 = temp2
		for s in p2p:
			if s > fTime:
				break
			window2.append(p2p.pop(0))
		for t in window2:
			i2 += 1
			
		try:
			tot = i1+i2
			if tot > 0:
				i1 = float(i1)/float(tot)
				i2 = float(i2)/float(tot)
			else:
				i1 = 0
				i2 = 0
			#Stampo sul file le cose per il grafico
			f1.write(str(fTime-absTime)+' '+str(i1)+'\n')
			f2.write(str(fTime-absTime)+' '+str(i2)+'\n')
		except Exception:
			print "ECCEZIONE",`sys.exc_info()`
			pass
		#Resetto le var
		iTime += unit
		fTime += unit
		temp1 = []
		i1 = 0
		temp2 = []
		i2 = 0
	f1.close()
	f2.close()
	
def FTIMEGraph(append, stats, size, unit, end, init):
	f = open(append+'FTIMEGraph','w')
	stats = sorted(stats, key=itemgetter(1))
	absTime = init
	iTime = init
	fTime = init + size
	i = 0.0
	window = []
	temp = []
	while iTime < end:
		for t in window:
			if t[1] > iTime:
				temp.append(t)
		window = temp
		for s in stats:
			if s[1] > fTime:
				break
			window.append(stats.pop(0))
		for t in window:
			i += t[0]
		try:
			i = i/len(window)
			#Stampo sul file le cose per il grafico
			f.write(str(fTime-absTime)+' '+str(i)+'\n')
		except Exception:
			#print "ECCEZIONE",`sys.exc_info()`
			pass
		#Resetto le var
		iTime += unit
		fTime += unit
		temp = []
		i = 0
	f.close()
	
def FTIMEGraph2(append, stats,size):
	f2 = open(append+'FTIMEGraph2cdf','w')
	times = []
	for s in stats:
		times.append(s[0])
	n = size/50
	probs = []
	for i in range(50):
		probs.append(0)
	for t in times:
		for i in range(50):
			if t > i*n and t <= i*n+n:
				probs[i] += 1
				break
			if i == 49:
				probs[49] += 1
	k = 0.0
	for i in range(50):
		k += float(probs[i])/float(len(times))
		f2.write(str(i*n+n/2)+' '+str(k)+'\n')
	f2.close()
	
def FailGraph(append, fail, size, unit, end, init):
	f1 = open(append+'FailGraph','w')
	fail.sort()
	absTime = init
	iTime = init
	fTime = init + size
	i1 = 0
	window1 = []
	temp1 = []
	while iTime < end:
		for t in window1:
			if t > iTime:
				temp1.append(t)
		window1 = temp1
		for s in fail:
			if s > fTime:
				break
			window1.append(fail.pop(0))
		for t in window1:
			i1 += 1
		
		try:
			tot = i1
			i1 = float(i1)
			#Stampo sul file le cose per il grafico
			f1.write(str(fTime-absTime)+' '+str(i1)+'\n')
			f1.flush()
		except Exception:
			print "ERROR",`sys.exc_info()`
		#Resetto le var
		iTime += unit
		fTime += unit
		temp1 = []
		i1 = 0
	f1.close()
	
def usage():
	print "USAGE:\n"
	print "\t-o | -output = output folder"
	print "\t-i | -input = input folder"
	print "\t-f = function to run"
	print "\t- = valore con cui scorre la finestra"
	
	print "\t-h | -help = display this\n"
	
def usage_func():
	print "Usable values for -f:\n"
	print "\t 1 : parse raw data"
	print "\t 2 : "
	print ""
		
if __name__ == "__main__":

	opts = None
	args = None	
	try:
		opts, args = getopt.getopt(sys.argv[1:], "ho:f:i:w:s:c:", ["help", "output=", "input="])
	except getopt.GetoptError, err:
		# print help information and exit:
		print str(err) # will print something like "option -a not recognized"
		usage()
		sys.exit(2)
	output = ""
	inpt = ""
	function = None
	wsize = None
	wslide = None
	cdfs = None
	
	for o, a in opts:
		if o == "-f":
			function = a.split(",")
		elif o in ("-h", "--help"):
			usage()
			sys.exit()
		elif o in ("-o", "--output"):
			output = a
		elif o in ("-i", "--input"):
			inpt = a
		elif o in ("-w",):
			wsize = float(a)
		elif o in ("-s",):
			wslide = float(a)
		elif o in ("-c",):
			cdfs = int(a)
		else:
			assert False, "unhandled option"
	
	if function == None:
		usage_func()
		sys.exit()
		
	stats = None
	nodes = None
	m = -1
	init = -1
	
	
	for f in function:
		if f == '1':
			#Genero i file con i valori divisi per tipologia
			nodes = getCepsfiles()
			stats = getStats(nodes)
			fl = open(output+"0", 'w')
			#Tempo di download totale
			s0 = sorted(stats[0], key=itemgetter(1))
			for v in s0:
				fl.write(`v[0]`+","+`v[1]`+"\n")
			fl.close()
			fl = open(output+"1", 'w')
			#Tempo di download primo chunk
			s1 = sorted(stats[1], key=itemgetter(1))
			for v in s1:
				fl.write(`v[0]`+","+`v[1]`+"\n")
			fl.close()
			#Scelto HTTP all'istante 
			stats[3].sort()
			s3 = stats[3]
			fl = open(output+"3", 'w')
			for v in s3:
				fl.write(`v`+"\n")
			fl.close()
			#Scelto P2P all'istante
			stats[4].sort()
			s4 = stats[4]
			fl = open(output+"4", 'w')
			for v in s4:
				fl.write(`v`+"\n")
			fl.close()
			#Ho contenuto in cache
			fl = open(output+"13", 'w')
			for v in s1:
				if v[0] == 0.001:
					fl.write(`v[1]`+"\n")
			fl.close()
			#404 da CDN
			stats[19].sort()
			s19 = stats[19]
			fl = open(output+"19", 'w')
			for v in s19:
				fl.write(`v`+"\n")
			fl.close()
			stats[14].sort()
			s14 = stats[14]
			fl = open(output+"14", 'w')
			for v in s14:
				fl.write(`v`+"\n")
			fl.close()
			stats[15].sort()
			s15 = stats[15]
			fl = open(output+"15", 'w')
			for v in s15:
				fl.write(`v`+"\n")
			fl.close()
			fl = open(output+"max", 'w')
			for i in stats[11]:
				if i > m:
					m = i
			fl.write(str(m))
			fl.close()
			fl = open(output+"init", 'w')
			init = stats[10][0]
			for i in stats[10]:
				if i < init:
					init = i
			fl.write(str(init))
			fl.close()
		elif f == '2':
			if not wsize or not wslide:
				usage()
				sys.exit()
			s = []
			if not stats:
				fl = open(inpt+"0", "r")
				for line in fl:
					l = line.split(",")
					s.append((float(l[0]),float(l[1])))
				fl.close()
			else:
				s = s0
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			RTIMEGraph(output, s, wsize, wslide, m+1, init)
		elif f == '3':
			if not cdfs:
				usage()
				sys.exit()
			s = []
			if not stats:
				fl = open(inpt+"0", "r")
				for line in fl:
					l = line.split(",")
					s.append((float(l[0]),float(l[1])))
				fl.close()
			else:
				s = s0
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			RTIMEGraph2(output, s, cdfs)
		elif f == '4':
			if not wsize or not wslide:
				usage()
				sys.exit()
			s = []
			if not stats:
				fl = open(inpt+"1", "r")
				for line in fl:
					l = line.split(",")
					s.append((float(l[0]),float(l[1])))
				fl.close()
			else:
				s = s1
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			FTIMEGraph(output, s, wsize, wslide, m+1, init)
		elif f == '5':
			if not cdfs:
				usage()
				sys.exit()
			s = []
			if not stats:
				fl = open(inpt+"1", "r")
				for line in fl:
					l = line.split(",")
					s.append((float(l[0]),float(l[1])))
				fl.close()
			else:
				s = s1
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			FTIMEGraph2(output, s, cdfs)
		elif f == '6':
			if not wsize or not wslide:
				usage()
				sys.exit()
			sa = []
			sb = []
			sc = []
			if not stats:
				fl = open(inpt+"3", "r")
				for line in fl:
					l = line.split(",")
					sa.append(float(l[0]))
				fl.close()
				fl = open(inpt+"4", "r")
				for line in fl:
					l = line.split(",")
					sb.append(float(l[0]))
				fl.close()
				fl = open(inpt+"13", "r")
				for line in fl:
					l = line.split(",")
					sc.append(float(l[0]))
				fl.close()
			else:
				sa = s3
				sb = s4
				sc = s13
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			TYPEGraphP2P(output, sa, sb, sc, wsize, wslide, m+1, init)
		elif f == '7':
			if not wsize or not wslide:
				usage()
				sys.exit()
			sa = []
			sc = []
			if not stats:
				fl = open(inpt+"3", "r")
				for line in fl:
					l = line.split(",")
					sa.append(float(l[0]))
				fl.close()
				fl = open(inpt+"13", "r")
				for line in fl:
					l = line.split(",")
					sc.append(float(l[0]))
				fl.close()
			else:
				sa = s3
				sc = s13
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			TYPEGraphNoP2P(output, sa, sc, wsize, wslide, m+1, init)
		elif f == '8':
			print "Not yet implemented"
			#TYPEGraphP2PChunk(append, stats[14], stats[15], float(sys.argv[2]), float(sys.argv[3]), m+1, init)
		elif f == '9':
			if not wsize or not wslide:
				usage()
				sys.exit()
			s = []
			if not stats:
				fl = open(inpt+"19", "r")
				for line in fl:
					l = line.split(",")
					s.append(float(l[0]))
				fl.close()
			else:
				s = s19
			if init == -1:
				fl = open(inpt+"init",'r')
				init = float(fl.readline())
				fl.close
				fl = open(inpt+"max",'r')
				m = float(fl.readline())
				fl.close
			FailGraph(output, s, wsize, wslide, m+1, init)
		elif f == '10':
			na = []
			nb = []
			nc = []
			if not stats:
				fl = open(inpt+"14", "r")
				na = len(fl.readlines())
				fl.close()
				fl = open(inpt+"15", "r")
				nb = len(fl.readlines())
				fl.close()
				fl = open(inpt+"13", "r")
				nc = len(fl.readlines())
				fl.close()
			else:
				na = len(s14)
				nb = len(s15)
				nc = len(s13)
			#prima 32 1600
			print "Chunks:", na, nb, nc*1600
			print "\nP2P-CDN:"
			print "Bytes:",((float(na)*32)/1024)/1024, "Euros:",(((float(na)*32)/1024)/1024) * 0.05
			print "\nCDN con cache:"
			print "Bytes:",(((float(na) + float(nb))*32)/1024)/1024, "Euros:",((((float(na) + float(nb))*32)/1024)/1024) * 0.05
			print "\nCDN:"
			print "Bytes:",((((float(na) + float(nb) + float(nc)*1600)*32)/1024)/1024), "Euros:",((((float(na) + float(nb) + float(nc)*1600)*32)/1024)/1024) * 0.05
		else:
			continue
	
	sys.exit()
