if ! mkdir ~/coast/tmp
then
	echo "Dir already existed";
fi
if ! mkdir ~/coast/tmp/cepcache
then
	echo "Dir already existed";
fi
cd ~/coast/src/coastkad/
if ! python setup.py build
then
	echo "ERROR";
	return 1;
fi
if ! sudo python setup.py install
then
	echo "ERROR";
	return 1;
fi
cd
cd ~/coast/src/cep/
if ! python setup.py build
then
	echo "ERROR";
	return 1;
fi
if ! sudo python setup.py install
then
	echo "ERROR";
	return 1;
fi
cd
cp ~/coast/utils/kconf.conf ~/coast/kconf.conf
cp ~/coast/utils/keys.db ~/coast/tmp/cepcache/
cp ~/coast/utils/cepConf.conf ~/coast/
cp ~/coast/utils/NodesCoordinates ~/coast/
if ! dd if=/dev/urandom of=/home/torinople_magnetto/coast/tmp/cepcache/cepTempFile bs=1024 count=32
then
	echo "ERROR";
fi
cp ~/coast/src/cepStart.py ~/
echo 'CEP INSTALLED SUCCESFULLY!';
