import SocketServer
import sys

class TesterHandler(SocketServer.BaseRequestHandler):
	"""
		The RequestHandler class for our server.
		It is instantiated once per connection to the server, and must
		override the handle() method to implement communication to the
		client.
	"""

	def handle(self):
		#self.request is the TCP socket connected to the client
		self.data = self.request.recv(1024)
		if self.data == 'ERROR':
			self.server.receivedError(self.client_address[0], self.data)
		else:
			self.server.receivedConfirmation(self.client_address[0], self.data)
		
		
class TesterServer(SocketServer.TCPServer):
	def __init__(self,addr,handler,f):
		SocketServer.TCPServer.__init__(self,addr,handler)
		self.f = f
		self.n = 0
		
	def receivedConfirmation(self,ip,hostname):
		self.f.write(hostname+'\n')
		print 'received confirmation from:', ip, hostname
		self.n+=1
		print 'received from:',self.n
		
	def receivedError(self, ip,hostname):
		print 'received error from:', ip, hostname
		self.n+=1
		print 'received from:',self.n

if __name__ == "__main__":
	HOST, PORT = "", 9999
	
	f = open(sys.argv[1],'w')
	
	#Create the server, binding to localhost on port 9999
	server = TesterServer((HOST, PORT), TesterHandler,f)
	
	#Activate the server; this will keep running until you
	#interrupt the program with Ctrl-C
	try:
		server.serve_forever()
	except KeyboardInterrupt:
		f.close()
		print '\n\n\033[41mbye bye\033[0m\n\n'
