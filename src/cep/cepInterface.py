"""Author: Francesco Bronzino
Description: module that implement the interface to the CEP core. It receives all content requests.
"""

from zope.interface import Interface
from zope.interface import implements
from twisted.internet.defer import Deferred
from twisted.internet import reactor
import time
import sys

#TODO: update ICEP interface

DEBUG = True

class ICEP(Interface):
	"""An abstract interface for using a DHT implementation."""
	
	def getContent(self, key):
		"""Get a content by key.
		
		Arguments:
		key -- object's key
		"""
		
	def finishedUsing(self, key):
		"""Inform the cep core that it doesn't need the content anymore
		
		Arguments:
		key -- object's key
		"""
		
class CepInterface:
	"""Class that implement the interface to the CEP core. It receives all content requests."""
	implements(ICEP)

	def __init__(self, core, logger, BUFSIZE = 5):
		"""Arguments:
		core -- CEP core instance
		logger -- logger instance
		BUFSIZE -- number of chunks needed to start the visualization of the video
		"""
		self.core = core
		self.logger = logger
		self.BUFSIZE = BUFSIZE
		self.requests = {} #funzione da chiamare per mandare il messaggio
		self.delivering = {} #mappa disponibile in cache
		self.times = {}
		self.lfirst = {}
		self.lbuf = {}
		
	def getContent(self, key):
		"""Get content request interface.
		
		Arguments:
		key -- object's key
		"""
		t = time.time()
		if self.delivering.has_key(key):
			self.core.getContent(key,self.updateFromCep, self.cepFinished)
			self.times[key].append(t)
		else:
			self.delivering[key] = self.core.getContent(key,self.updateFromCep, self.cepFinished)
			self.times[key] = []
			self.times[key].append(t)
			self.lfirst[key] = False
			self.lbuf[key] = False
		df = Deferred()
		self.requests.setdefault(key, []).append(df)
		try:
			if self.delivering[key]['map'] and self.delivering[key]['map'].getTot() >= 1:
				self.logger.logFirstTime(0.001,t)
				self.lfirst[key] = True
			if self.delivering[key]['map'] and self.delivering[key]['map'].getInit() == self.BUFSIZE:
				self.lbuf[key] = True
				self.logger.logVideoInitTime(0.001,t)
			if self.delivering[key]['map'] and self.delivering[key]['map'].getTot() == self.delivering[key]['size']:
				self.logger.logCacheConn(t)
				reactor.callFromThread(self.cepFinished,key)
		except TypeError:
			if DEBUG: print "CepInterface: content not present in system, started retrieval"
			return df
		except Exception:
			if DEBUG: print "CepInterface ERROR:", `sys.exc_info()`
			return df
		return df
		
	def finishedUsing(self, key):
		"""Used to inform the interface that a content is not being more delivered.
		
		Arguments:
		key -- object's key
		"""
		if not self.delivering.has_key(key):
			self.core.finishedDelivering(key)
		
	def updateFromCep(self, key, vals):
		"""CEP core informs that new chunks has been retrieved
		
		Arguments:
		key -- object's key
		vals -- dictionary [map = bitmap of the content, [OTHERS UNUSED]]
		"""
		self.delivering[key] = vals
		actt = time.time()
		if not self.lfirst[key] and self.delivering[key]['map'] and self.delivering[key]['map'].getTot() >= 1:
			if DEBUG: print "CepInterface: first chunk available for:",key
			for t in self.times[key]:
				self.logger.logFirstTime(actt-t,actt)
				self.lfirst[key] = True
		if not self.lbuf[key] and self.delivering[key]['map'] and self.delivering[key]['map'].getInit() == self.BUFSIZE:
			if DEBUG: print "CepInterface: buffer available for:",key
			for t in self.times[key]:
				self.lbuf[key] = True
				self.logger.logVideoInitTime(actt-t,actt)
		
	def cepFinished(self,key):
		"""CEP core informs that the content has been retrieved.
		
		Arguments:
		key -- object's key
		"""
		for client in self.requests[key]:
			reactor.callFromThread(client.callback, self.delivering[key])
		#actt = time.time()
		#for t in self.times[key]:
		#	pass
		try:
			del self.requests[key]
			del self.delivering[key]
			del self.times[key]
			del self.lfirst[key]
			del self.lbuf[key]
		except Exception:
			if DEBUG: print "CepInterface ERROR: while cleaning", `sys.exc_info()`
		self.core.finishedDelivering(key)
