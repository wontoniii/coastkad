"""
	This module provide the http interface (server) provided by a cep node
	Python's module are extended to offer the funtionalities requeried by the the cep architecture
"""


import SimpleHTTPServer
import BaseHTTPServer
import SocketServer
from cep.db import DB
from cep.utils import createKey, divKey
import os
import sys
import time
from SocketServer import ThreadingMixIn
import threading

DEBUG = True

class CepHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	"""
		Class that extend SimpleHTTPRequestHandler and add the functionalities required by a cep node
	"""
	def __init__(self, request, client_address, server):
		"""
			* request: request object provided in the parent class
			* client_address: the address of the client requestingn the content
			* server: CepHTTPServer object to communicate with
		"""
		SimpleHTTPServer.SimpleHTTPRequestHandler.__init__(self,request, client_address, server)
		
	def do_GET(self):
		"""
			Handle a get request
		"""
		#Devo introdurre quello che voglio prima di chiamare la do_GET della classe padre
		#Questo mi permetterebbe di aggiungere quello che voglio
		#Per il momento uso sempre lo stesso file: cdnTempFile
		ctype = self.guess_type(self.path)
		try:
			key, chunk = divKey(self.path)
		except Exception:
			self.send_error(404, "File not found")
			return
		key = key.lstrip('/')
		self.path,bitmap = self.server.getFilename(key,chunk) 
		if not self.path:
			if DEBUG: 'CepHandler: File not found in the db'
			self.send_error(404, "File not found")
			return
		self.path = self.server.getServerPath() + self.path
		try:
			#Always read in binary mode. Opening files in text mode may cause
			# newline translations, making the actual size of the content
			# transmitted *less* than the content-length!
			f = open(self.path, 'rb')
		except IOError:
			if DEBUG: print 'CepHandler ERROR: file not found', self.path
			self.send_error(404, "File not found")
		else:
			if DEBUG: print "CepHandler: requested file:",self.path
			ctype = self.guess_type(self.path)
			self.send_response(200)
			self.send_header("Content-type", ctype)
			fs = os.fstat(f.fileno())
			self.send_header("Content-Length", str(fs[6]))
			self.send_header("Content-bitmap", bitmap)
			self.end_headers()
			self.copyfile(f, self.wfile)
			f.close()
			self.close_connection = 1
			
	def do_HEAD(self):
		"""
			Handle a head request
		"""
		key = self.path.lstrip('/')
		self.path,bitmap = self.server.getFilename(key,-1) 
		if not self.path:
			if DEBUG: 'CepHandler: File not found in the db'
			self.send_error(404, "File not found")
			return
		self.path = self.server.getServerPath() + self.path
		if DEBUG: print "CepHandler: requested header for file:",self.path
		ctype = self.guess_type(self.path)
		self.send_response(200)
		self.send_header("Content-type", ctype)
		self.send_header("Content-bitmap", bitmap)
		self.end_headers()
		self.close_connection = 1
			
	def do_POST(self):
		"""
			Handle a post request
		"""
		self.send_error(404, "File not found")
		self.close_connection = 1


class CepHTTPServer(ThreadingMixIn, BaseHTTPServer.HTTPServer):
	"""
		Extend a normal python HTTP server. This let the cep node to manage various things
	"""
	def setup(self, serverPath, db, logger, filename = None):
		"""
			* server_address
			* serverPath
			* db
			* filename
			* logger
			* initT
		"""
		self.serverPath = serverPath
		try:
			os.stat(self.serverPath)
		except:
			os.makedirs(self.serverPath)
		self.dbFile = db
		self.filename = filename
		self.logger = logger
		self.request_queue_size = 8192
		if filename:
			self.getFilename = self.getConst
			
	def serve_forever(self):
		#self.db = DB(self.dbFile)
		BaseHTTPServer.HTTPServer.serve_forever(self)
		
	def getServerPath(self):
		"""
			return the value of serverPath
		"""
		return self.serverPath
		
	def getFilename(self, key, chunk):
		"""
			Return the filename corresponding to the key and the current bitmap
		"""
		#Mettiamo i dati in un database, in modo da non dover avere una corrispondenza diretta tra chiave e nome del file
		db = DB(self.dbFile)
		l = db.retrieveValue(key)
		db.close()
		if not l:
			if DEBUG: print 'cepHTTPServer: key',key,' not found'
			return (None,None)
		if DEBUG: print 'cepHTTPServer:',key,'-->',l[1]
		if chunk >= 0:
			f = createKey(l[1],chunk)
			t = time.time()
			self.logger.logCEPChunk(t)
		return (f,l[2])
		
		
	def getConst(self,key, chunk):
		"""
			Return the filename corresponding to the key and the current bitmap it returns always the same file
			Only for tests purposes
		"""
		db = DB(self.dbFile)
		l = db.retrieveValue(key)
		db.close()
		if not l:
			if DEBUG: print 'cepHTTPServer: key',key,' not found'
			return (None,None)
		t = time.time()
		self.logger.logCEPChunk(t)
		return (self.filename,l[2])
		
		
		
		
		
		
		
		
if __name__== "__main__":
	db = DB('cache.db')
	db.storeValue('1', 1600, '', 12153216433574357347)
	from cep.statsLogger import StatsLogger
	logger = StatsLogger('templog')
	logger.startLogging(0)
	server_address = ('', 10111)
	httpd = CepHTTPServer(server_address, CepHandler)
	httpd.setup("/tmp/", "cache.db", logger, filename = "cepTempFile")
	httpd.serve_forever()
