"""
	Interface with the dht
	Two ways of comunication:
		-cep node is coresident with the dht node
		-cep node knows a list of dht possible nodes
"""

#TODO:
#fare in modo che sia un po' piu' fault resistant
#valutare il caso in cui alla risposta non contenga valori(in particolare per la store. Per la get e' giusto funzioni cosi, per la store un po meno

from twisted.internet.defer import Deferred
from twisted.internet.protocol import ClientFactory,Protocol
from twisted.internet import reactor
from cep.dhtProtocol import DhtFactory

DEBUG = False

class DhtInterface:
	"""
		An interface for the cep and the ccl to interact with the dht.
		If a dht node is working on the same machine it simply interact with it.
		If not, a list of nodes is given and all the interface will try to contact the dht from one of this nodes
	"""
	def __init__(self, dht= None, nodes = None):
		"""
			* dht: object representing a dht node (co-resident with the cep node)
			* nodes: list of dht nodes (remote connection to the dht)
		"""
		self.dht = None
		self.nodes = None
		if dht:
			self.dht = dht
			self.getValue = self.getValueDht
			self.setValue = self.setValueDht
		elif nodes:
			self.dht = DhtFactory(nodes[0], self.connectionMade, self.connectionDone) #Creare l'interfaccia di rete
			self.nodes = nodes #self.setUp(nodes)
			self.getValue = self.getValueNet
			self.setValue = self.setValueNet
		else:
			raise Error
		self.waitingSet = {}
		self.waitingGet = {}
			
	#Scrivere il sistema di timeout per fare in modo di inviare le richiesta
	
	def connectionMade(self):
		"""
			Announce that the connection has been established
		"""
		
	def connectionDone(self, reason):
		"""
			Announce that the connection is done
		"""
			
	def getValueDht(self, key):
		"""Contact the local node and ask for the value associated to the key"""
		if DEBUG: print 'DhtInterface: Get value:',key
		if not self.waitingGet.has_key(key):
			df = self.dht.getValue(key)
			df.addCallback(self._getValue)
			df.addErrback(self._getValueError)
		else:
			if DEBUG: print 'DhtInterface: There is already a request fo the same key', key
		df = Deferred()
		#print self.waitingGet
		self.waitingGet.setdefault(key, []).append(df)
		return df
		
	def getValueNet(self, key):
		"""Contact one of the nodes in the nodes list and try to contact them"""
		if not self.waitingGet.has_key(key):
			#Ok devo decidere come contattare la dht
			#self.waitingGet[key] = []
			if DEBUG: print 'Get value :',key
			df = self.dht.getValue(key)
			df.addCallback(self._getValue,key)
		else:
			if DEBUG: print 'There is already a request fo the same key'
		df = Deferred()
		self.waitingGet.setdefault(key, []).append(df)
		return df
		
	def _getValue(self,(key,rep)):
		"""Once the information has been recovered it return the values to the ccl"""
		#Volendo posso preparare l'informazione affinche' sia piu' pulita per la dht
		#Devo decidere se voglio avere indietro tutto il messaggio o solo i valori
		#Per il momento direi che si passano solo i valori, ma da li' in poi vedremo
		#Soluzione provvisoria per eliminare cio' che e' stato aggiunto per normalizzare la chiave dalla dht
		#Forse sarebbe meglio di denormalizzare prima...
		if DEBUG: print 'DhtInterface: Received reply from dht',key,rep
		key = key.rstrip('\000')
		if not self.waitingGet.has_key(key):
			if DEBUG: print 'DhtInterface ERROR: received a key that doesnt exist'
			return
		for i in range(len(self.waitingGet[key])):
			d = self.waitingGet[key].pop(0)
			reactor.callFromThread(d.callback,(key, rep))
		del self.waitingGet[key]
		
	def _getValueError(self,fkey):
		"""Once the information has been recovered it return the values to the ccl"""
		if DEBUG: print 'DhtInterface ERROR: Received error reply from dht:',fkey
		key = fkey.value
		#Soluzione provvisoria per eliminare cio' che e' stato aggiunto per normalizzare la chiave dalla dht
		#Forse sarebbe meglio di denormalizzare prima...
		key = str(key).rstrip('\000')
		if not self.waitingGet.has_key(key):
			if DEBUG: print 'DhtInterface ERROR: received a key that doesnt exist'
			return
		for i in range(len(self.waitingGet[key])):
			d = self.waitingGet[key].pop(0)
			reactor.callFromThread(d.callback,(key, []))
		del self.waitingGet[key]
		
	def setValueDht(self, key, value):
		"""Contact the local node and store the value associated to the key"""
		if not self.waitingSet.has_key(key):
			#Ok devo decidere come contattare la dht
			if DEBUG: print 'DhtInterface: Set value:',key,value
			df = self.dht.storeValue(key, value)
			df.addCallback(self._setValue)
		df = Deferred()
		self.waitingSet.setdefault(key, []).append(df)
		return df
		
	def setValueNet(self, key, value):
		"""Contact one of the nodes in the nodes list and try store the value associated to the key"""
		if not self.waitingSet.has_key(key):
			#Ok devo decidere come contattare la dht
			df = self.dht.setValue(key, value)
			df.addCallback(self._setValue)
		df = Deferred()
		self.waitingSet.setdefault(key, []).append(df)
		return df
		
	def _setValue(self,(key,value)):
		"""Once the information has been stored it return the positive response"""
		#Soluzione provvisoria per eliminare cio' che e' stato aggiunto per normalizzare la chiave dalla dht
		#Forse sarebbe meglio di denormalizzare prima...
		key = key.rstrip('\000')
		if DEBUG: print 'DhtInterface: set value response received from the dht',key,value
		if not self.waitingSet.has_key(key):
			if DEBUG: print 'DhtInterface ERROR: received a key that doesnt exist'
			return
		for i in range(len(self.waitingSet[key])):
			d = self.waitingSet[key].pop(0)
			reactor.callFromThread(d.callback,(key,value))
		del self.waitingSet[key]
		
		
		
class test:
	def __init__(self):
		from coastkad import kDht
		from coastkad.kconf import config
		self.dht = kDht.KDHT()
		self.dht.loadConfig(config, config.get('DEFAULT', 'DHT'))
		self.dht.join()
		self.dhtI = DhtInterface(dht = self.dht)
		
	def startTest(self):
		reactor.callLater(3,self.op1)	
		
	def op1(self):
		print '\n\n Operazione 1'
		df = self.dhtI.setValue('provasi',{'address':('127.0.0.1',8888),'flag':11111111111})
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(2,self.op2)
		
	def op2(self):
		print '\n\n Operazione 2'
		df = self.dhtI.getValue('provasi')
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(2,self.op3)
		
	def op3(self):
		print '\n\n Operazione 3'
		df = self.dhtI.setValue('provasi',{'address':('127.0.0.1',8888),'flag':22222222})
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(2,self.op4)
		
	def op4(self):
		print '\n\n Operazione 4'
		df = self.dhtI.getValue('provasi')
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(2,self.op5)
		
	def op5(self):
		print '\n\n Operazione 5'
		df = self.dhtI.setValue('provano', 'valore2')
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(2,self.op6)
		
	def op6(self):
		print '\n\n Operazione 6'
		df = self.dhtI.getValue('provano')
		df.addCallbacks(self.cal,self.err)
		reactor.callLater(3,self.finished)
		
	def cal(self, (key,val)):
		print key,val
		
	def err(self, fail):
		print fail
		
	def finished(self):
		reactor.stop()
		
if __name__=='__main__':
	t = test()
	t.startTest()
	reactor.run()
