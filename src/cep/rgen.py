"""
	Module that implements the random generators for the distributions that are not included in Random class
"""

from random import Random
import time
import math

class Zipf(Random):
	"""
		Zipf random number generator
	"""
	
	def __init__(self, (slope, size), x=None):
		Random.__init__(self, x)
		self.slope = float(slope)
		self.size = int(size)
		self.bottom = float(0)
		for i in range(self.size-1):
			self.bottom += 1.0/((i+1.0)**self.slope)
		self.getNext = self.randZRank
		
	def randZRank(self):
		"""
			Get the next rank instance based on a ZipF distribution
		"""
		rank = self.randint(1,self.size)
		frequency = (1.0/(rank**self.slope))/self.bottom
		dice = self.random()
		while not dice<frequency:
			rank = self.randint(1,self.size)
			frequency = (1.0/(rank**self.slope))/self.bottom
			dice = self.random()
		return rank
		
	def getProbability(self, rank):
		"""Get the probability to excract a given rank"""
		return (1.0/(rank**self.slope))/self.bottom
		
				
class Uniform(Random):
	"""
		Uniform random number generator
	"""
	
	def __init__(self, (a, b), x=None):
		Random.__init__(self, x)
		self.a = a
		self.b = b
		self.getNext = self.random
	
	def random(self):
		"""Get the next instance of the Uniform"""
		n = Random.random(self)
		return int(self.a+(self.b-self.a)*n)
		
class Normal48(Random):
	"""
		Gaussian random number generator
	"""
	def __init__(self, (mean, variance), x=None):
		Random.__init__(self, x)
		self.mean = mean
		self.var = variance
		self.getNext = self.random
	
	def random(self):
		ret = -1
		while(ret<0):
			ret = 0
			for i in range(48):
				ret += Random.random(self)
			ret = self.var*(ret-24)+self.mean
		return ret
		
class ExpNeg(Random):
	"""
		Negative exponential distribution
	"""
	def __init__(self, mu, x=None):
		Random.__init__(self, x)
		self.mu = mu
		self.getNext = self.random
		
	def random(self):
		ret = 0
		y = Random.random(self)
		ret =-(math.log(1-y)/self.mu);
		return ret

class ArrayComposition(Random):
	"""
		Discrete probability generator
	"""
	def __init__(self, (probs,vals), x = None):
		Random.__init__(self, x)
		self.probs = probs
		self.vals = vals
		self.n = len(vals)
		self.getNext = self.randComp
	
	def randComp(self):
		y = self.random()
		count = self.probs[0]
		for i in range(self.n):
			if y < count:
				return self.vals[i]
			count += p[i+1]

class Const(Random):
	"""
		Class that just return a constant
	"""
	def __init__(self, const, x=None):
		self.const = const
		self.getNext = self.random
		
	def random(self):
		return self.const
		
				
if __name__=='__main__':
	r = ExpNeg(0.2)
	ar = []
	for i in range(10001):
		ar.append(0)
	for i in range(100000):
		next = r.randZRank()
		ar[next] += 1
	for i in range(len(ar)):
		print ar[i]
