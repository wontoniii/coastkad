"""
	Classes used to connect and to interact with a COAST dht node in case there is not co-resident dht node
"""


from twisted.internet.defer import Deferred
from twisted.internet.protocol import ClientFactory,Protocol
from twisted.internet import reactor

DEBUG = True

#TODO: completare il caso in cui i nodi dht siano remoti
#This class should be used if the there is not a dht node in the same machine
#Not complete!!!


class DhtProtocolTCP(Protocol):
	"""
		Protocol used to communicate with a dht node
	"""
	def __init__(self, client):
		self.client = client
	
	def connectionMade(self):
		log.msg('Connection made')
		self.client.clientConnectionMade()
		
	def dataReceived(self, data):
		self.client.gotResponse(data)
		
	def sendMessage(self, req):
		self.transport.write(req)
		
	def closeConnection(self):
		log.msg('Closing connection')
		
class DhtFactory(ClientFactory):
	def __init__(self,node, callback, errback):
		self.connected = False
		self.failed = False
		#Nodo a cui e' connesso
		self.node = node
		#Dizionario dove sono salvati i deferred a cui rispondere alle richieste di 
		#Le chiavi sono i MID
		#I valori sono una lista di deferred a cui restituire il valore richiesto
		self.queries = {}
		
	def startedConnecting(self, connector):
		msg = 'Dht interface is trying to connect to: '+`self.node`
		log.msg(msg)
		
	def clientConnectionMade(self):
		self.connected = True
		self.failed = False
		#Next call was made to advise the caller that the connection had been established
		reactor.callFromThread(self.callback,self.node)
		
	def clientConnectionFailed(self, connector, reason):
		msg = 'Connection with '+`connector.getDestination()`+'has failed because of: '+`reason`
		log.err(msg)
		self.failed = True
		self.connected = False
		#Next call was made to advise the caller that the connection had failed
		reactor.callFromThread(self.errback,self.node,reason)
		
	def clientConnectionLost(self, connector, reason):
		msg = 'Connection with '+`connector.getDestination()`+'has been lost because of: '+`reason`
		log.err(msg)
		self.failed = True
		self.connected = False
		#Next call was made to advise the caller that the connection had failed
		reactor.callFromThread(self.errback,self.node,reason)
	
	def clientConnectionClose(self):
		"""
			This function should close the connection with the dht node
		"""
		#Non deve chiamare indietro niente, deve solo chiudere la connessione nel caso non mi serva piu'
		msg = 'Connection with '+`node`+' is getting closed'
		log.msg(msg)
		self.connected = False
		
	def buildProtocol(self, addr):
		self.protocol = DhtProtocolTCP(self)
		return self.protocol
		
	def gotResponse(self, msg):
		"""Store a received response in a file"""
		mtime = time.time()
		rep = bdecode(msg)
		df = self.queries.pop(rep['mid'])
		reactor.callFromThread(df.callback, rep)
		del df
		
	def isConnected(self):
		return self.connected
		
	def hasFailed(self):
		return self.failed
		
	def sendRequest(self,req):
		df = Deferred()
		self.queries[req['mid']] = df
		self.protocol.sendMessage(bencode(req))
		return df
