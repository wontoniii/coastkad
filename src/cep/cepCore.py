"""Author: Francesco Bronzino
Description: This module represent the main core of a cep node
"""

###TODO: mettere un timeout per la ripubblicazione nella dht dei propri contenuti

DEBUG = True

from twisted.internet.defer import Deferred
from twisted.internet import reactor
from cep.cepCcl import Ccl
from cep.cepCache import CepCache
from cep.webClient import CepWebClient
from cep.filesystem import Filesystem
from cep.utils import *
from cep.retrieve import RetrieveHTTP, RetrieveP2P
from coastkad import kDht
from coastkad.kconf import config
import random
import time
import sys

class CepCore:
	"""Core of a cep node. Organize all the actions that a cep node has to do
	"""
	def __init__(self, cache, clientHTTP, ccl, address, idht, logger, P2PCONC = 3, REFT = 60, TOUT = 60, BUFSIZE = 5, ACTP2P = True):
		"""Arguments:
		cache -- cache instance
		clientHTTP -- HTTP client instance used to do all the HTTP GETS
		ccl -- Content Cache Locator instance
		address -- node's address
		idht -- dht client interface instance
		logger -- logger instance (used to log events)
		P2PCONC -- number peers to use in P2P
		REFT -- refresh time used in retrieval
		TOUT -- timeout time used in retrieval
		BUFSIZE -- number of chunks considered enough to start reproducing the video
		ACTP2P -- boolean, true if P2P is usable
		"""
		self.ccl = ccl#Inizializzare nella maniera corretta la ccl
		self.cache = cache #Inizializzare i maniera corretta la cache
		self.clientHTTP = clientHTTP #Inizializzare bene il client http
		self.retrieving = {} #{key:array of filename,bitmap} Rappresenta un contenuto che sta venendo recuperato
		self.delivering = {} #{key:bool} Rappresenta un contenuto che sta venendo distribuito dall'interfaccia sottostante
		self.update = {} #rappresenta la funzione da chiamare quando si hanno update riguardanti il retrieval dei contenuti
		self.finished = {} #Rappresenta la funzione da chiamare quando si il retrieval del contenuto e' stato ultimato
		self.times = {}
		self.P2PCONC = P2PCONC
		self.address = address
		self.dht = idht
		self.logger = logger
		self.REFT = REFT
		self.TOUT = TOUT
		self.BUFSIZE = BUFSIZE
		self.ACTP2P = ACTP2P
		reactor.callLater(60*5,self.logNCachedContents)
		
	def getContent(self, key, update, finished):
		"""Start the delivery of a content. If the content is in the cache just return it's map. If it's not starts the retrieve process.
		
		Arguments:
		key -- object's key
		update -- function to call when a new chunk is retrieved
		finished -- function to call when the entire content has been retrieved
		
		Return:
		The bitmap of the file if it is already at least partially cached and is not been retrieved, None otherwise
		"""
		#La funzione deve ritornare come recuperare il file dal sistema operativo.
		#Sara' poi un altro modulo ad occuparsi del delivery.
		#Quindi prima di tutto ritorna cosa ho del file in memoria.
		#Ritorna None se non ho niente.
		#Aggiunge poi il fatto che il client vuole che gli ritorni aggiornamenti sullo stato del buffer.
		t = time.time()
		if DEBUG: print 'CepCore: Starting the content location'
		if self.delivering.has_key(key):
			#So che l'interfaccia sottostante sta gia' consegnando il contenuto, aggiorno solo il timestamp in cache
			if DEBUG: print 'CepCore: Devo solo aggiornare il timestamp in cache perche sto gia mandando updates sulla disponibilita.'
			self.cache.updateValue(key)
			#Il valore ritornato non basta sia un solo None, deve essere una lista di None
			return None
		#Recupero quello che c'e' dalla cache
		self.update[key] = update
		self.finished[key] = finished
		val = self.cache.getByKey(key)
		if val:
			#Ho il valore in cache, ma non lo sto recuperando dalla rete. In una situazione normale ho l'intero contenuto in memoria. In caso contrario siginfica che c'e' stato qualche errore.
			if DEBUG: print 'CepCore: Ho il valore in cache e quindi gestisco la richiesta'
			#Ho il valore in cache, basta che ritorno la bit map e le altre informazioni utili per il delivery
			self.cache.lockValue(key)
			#Credo sia meglio avere una sola interfaccia, al livello sotto si occupa di distribuire i file
			self.delivering[key] = 1
			#Decidere se si vuole ritornare qualcosa di diverso
			return val
		#Altrimenti non ho ancora niente in cache e quindi devo recuperare il contenuto
		else:
			if DEBUG: print 'CepCore: non ho il valore in cache, inizio a localizzarlo con la ccl'
			self.times[key] = t
			self.delivering[key] = 1
			self.retrieving[key] = {}
			waitd = self.ccl.startLocation(key)
			waitd.addCallback(self.cclResponse)
			waitd.addErrback(self.cclError)
		#Il valore ritornato non basta sia un solo None, deve essere una lista di None
		return None
		
	def finishedDelivering(self, key):
		"""The cep is informed that the content key is not being delivered anymore.
		
		Arguments:
		key -- object's key
		"""
		if DEBUG: print 'Content',key,'is no more delivered'
		#Elimino la chiave dalla lista dei contenuti in delivery
		if self.delivering.has_key(key):
			del self.delivering[key]
		if self.update.has_key(key):
			del self.update[key]
		if self.finished.has_key(key):
			del self.finished[key]
		#Se ho finito il recupero allora posso sbloccare il contenuto e liberarlo dalla cache
		if not self.retrieving.has_key(key):
			self.cache.unlockValue(key)
		
	def cclResponse(self, (key, method, values, size)):
		"""Once that the ccl has established the method to use for retrieving the information, we has to procede to stream the video.
		
		Arguments:
		key -- object's key
		method -- HTTP or P2P or CDN (CDN deprecated)
		values -- list of CDN or peer nodes
		size -- size of the content to be retrieved
		"""
		if DEBUG: print 'CepCore: Ricevuta risposta dalla ccl: ',key,method,values,size
		#Analizza la risposta e procede a inviare il contenuto. Decidere come questo va avanti
		#Metterli in ordine di importanza
		#Una cosa che potrebbe essere aggiunta e' il setup della bitmap, nel caso in cui stia riprendendo un download
		#Aggiungere il file alla cache in modo da fare spazio per il download
		#Ci sono due cose che devo guardare 
		#decidere con che nomi salvare i file
		self.retrieving[key]['size'] = size
		#Non ho bisogno di un nome reale perche' tanto per il momento non lavoro con il filesystem
		self.retrieving[key]['filename'] = 'QUALSIASI'
		#Inizialmente non ho nessun file
		self.retrieving[key]['map'] = Bitmap( m = 0)
		#come ottenere la dimensione del file. Per il momento supponiamo una cache infinita e quindi non ci devo perdere troppo tempo
		try:
			#key, size, filename, m, buf
			self.cache.insert(key, size, self.retrieving[key]['filename'], self.retrieving[key]['map'], [])
			retr = None
			t = time.time()
			if method == HTTP:
				if DEBUG: print 'CepCore: Recupero il valore tramite HTTP'
				self.logger.logHTTPConn(t)
				retr = RetrieveHTTP(self.clientHTTP, key, self.retrievalUpdate, self.retrievalUpdateError, self.updateSize, values, self.logger, size = 1, tout = self.TOUT)
			if method == P2P:
				if DEBUG: print 'CepCore: Recupero il valore tramite P2P'
				self.logger.logP2PConn(t)
				retr = RetrieveP2P(self.clientHTTP, key, self.retrievalUpdate, self.retrievalUpdateError, values, self.logger, par = self.P2PCONC, size = size, REFT = self.REFT, tout = self.TOUT)
			elif method == CDN:
				if DEBUG: print 'CepCore: Recupero il valore dalla CDN'
				self.logger.logHTTPConn(t)
				retr = RetrieveHTTP(self.clientHTTP, key, self.retrievalUpdate, self.retrievalUpdateError, self.updateSize, values, self.logger, size = 1, tout = self.TOUT)
			if DEBUG: print 'CepCore: Ready to launch the retrieval'
			df = retr.startRetrieving()
			df.addCallback(self.retrieveComplete)
			df.addErrback(self.retrieveCompleteError)
		except Exception:
			if DEBUG: print 'CepCore ERROR: during the retrieval starting for key:',key,`sys.exc_info()`
		
	def cclError(self,failure):
		"""There have been an error while locating the sources for the retrieval.
		
		Arguments:
		failure -- failure's reason
		"""
		if DEBUG: print 'CepCore: Error while locating the cache'
		#In questo caso probabilente l'unica soluzione sarebbe ripartire da capo...
		
	def updateSize(self, key, size):
		"""Update the size of the content in cache.
		
		Arguments:
		key -- object's key
		size -- new size of the object
		"""
		self.retrieving[key]['size'] = size
		self.cache.updateSize(key,size)
		
	def retrievalUpdate(self, key, chunk, buf):
		"""A new chunk has been retrieved.
		
		Arguments:
		key -- object's key
		chunk -- number of the new chunk
		buf -- object instance representing the buffered chunk
		"""
		if DEBUG: print "CepCore: new chunk retrieved:",key, chunk
		#if self.retrieving[key]['map'].getTot() == 0:
		#	t = time.time()
		#	self.logger.logFirstTime(t-self.times[key],t)
		#Aggiorno la bitmap del file che sto recuperando
		self.retrieving[key]['map'].setBit(chunk)
		#if self.retrieving[key]['map'].getInit() == self.BUFSIZE:
		#	t = time.time()
		#	self.logger.logVideoInitTime(t-self.times[key],t)
		#Mando indietro l'informazione da aggiornare a chi mi ha fatto la richiesta
		reactor.callFromThread(self.update[key],key,self.retrieving[key])
		#Aggiorno la cache
		self.cache.updateMap(key, self.retrieving[key]['filename'], self.retrieving[key]['map'], buf)
		#A questo punto bisognerebbe anche decidere quando aggiornare la dht
		m = genFlag(0, self.retrieving[key]['map'].getInit(), self.retrieving[key]['map'].getTot())
		#Manca l'indirizzo
		if self.ACTP2P:
			self.dht.setValueDht(key, {'address':self.address,'flag':m})
		
	def retrievalUpdateError(self, key, chunk):
		"""An error has occurred while retrieving the chunk and can not be resolved.
		
		Arguments:
		key -- object's key
		chunk -- number of the chunk
		"""
		if DEBUG: print 'CepCore: ERROR retrieving a chunk'
		#In questo caso si dovrebbero prenere delle contromisure, tipo trovare nuove fonti da cui recuperare il contenuto
		
	def retrieveComplete(self, key):
		"""Once the content has been completely retrieved the cep core is noticed.
		The cep core store the new value in the cache.
		
		Arguments:
		key -- object's key
		"""
		#Controllo che non ci sia stato un errore e se tutto ok elimino l'informazione
		if DEBUG: print "CepCore: retrieve complete:",key
		reactor.callFromThread(self.finished[key],key)
		m = genFlag(1, self.retrieving[key]['map'].getInit(), self.retrieving[key]['map'].getTot())
		if self.ACTP2P:
			self.dht.setValueDht(key, {'address':self.address,'flag':m})
		if self.retrieving.has_key(key):
			del self.retrieving[key]
		if not self.delivering.has_key(key):
			self.cache.unlockValue(key)
		if self.update.has_key(key):
			del self.update[key]
		if self.finished.has_key(key):
			del self.finished[key]
		
	def retrieveCompleteError(self, key):
		"""Once the content has been completely retrieved the cep core is noticed.
		In the casse some part of the information has not been retrieved.
		
		Arguments:
		key -- object's key
		"""
		#Il file non e' stato recuperato correttamente
		#Bisognerebbe fare in modo che si riprovi o qualcosa del genere
		#Per il momento si comporta come se avesse ottenuto l'intera informazione
		if DEBUG: print "CepCore: retrieve complete but with something missing:",key
		reactor.callFromThread(self.finished[key],key,self.retrieving[key]['map'])
		if self.retrieving.has_key(key):
			del self.retrieving[key]
		if not self.delivering.has_key(key):
			self.cache.unlockValue(key)
		if self.update.has_key(key):
			del self.update[key]
		if self.finished.has_key(key):
			del self.finished[key]
			
	def logNCachedContents(self):
		"""Log the number of contents cached at time t."""
		t = time.time()
		self.logger.logNContents(t, self.cache.getNContents())
		reactor.callLater(60*5,self.logNCachedContents)
		


class test:
	def __init__(self, address, bufferPath):
		dht = kDht.KDHT()
		dht.loadConfig(config, config.get('DEFAULT', 'DHT'))
		dht.join()
		filesystem = Filesystem('temp/')
		core = CepCore(address, dht, filesystem, bufferPath, self.update)
		self.interface = CepInterface(core)
		
	def startTest(self):
		reactor.callLater(2,self.run)
		
	def run(self):
		m = self.interface.getContent('prova')
		print 'START: ',m
		
	def update(self, key, m):
		print 'UPDATE:', key, m
		
	def finished(self):
		reactor.stop()
		
if __name__=='__main__':
	t = test('localhost', 'temp/')
	t.startTest()
	reactor.run()
