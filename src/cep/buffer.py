"""Author: Francesco Bronzino
Description: Module that implements a buffer used to store a temporary file during the retrieval
"""

import sys

class Buffer:
	"""Class that create a buffer to store some information.
	It stores the information into a file.
	"""
	def __init__(self, filename):
		"""Arguments:
		filename -- the filename of the file used to store the information
		"""
		self.filename = filename
		self.f = open(self.filename, 'w')
		
	def append(self, data):
		"""Appends some datas to the file.
		
		Arguments:
		data -- bytes to add
		"""
		self.f.write(data)
		
	def getData(self):
		"""Function that return all the content of the buffer.
		
		Return:
		content of the buffer
		"""
		self.f.close()
		self.f = open(self.filename, 'r')
		ret = ''
		for line in self.f:
			ret += line
		self.f.close()
		self.f = open(self.filename, 'a')
		return ret
		
	def close(self):
		"""Closes the file used to store the buffer."""
		self.f.close()
		del self.f
		
	def isClosed(self):
		"""Checks if the buffer is closed."""
		return self.f == None
		
	def getName(self):
		"""Return the name of the file used to store the buffer.
		
		Return:
		the name of the file
		"""
		return self.filename
