"""Author: Francesco Bronzino modified from apt-p2p config files
Description: module that loads settings from a setting file
"""

"""Loading of configuration files and parameters.

@type version: L{twisted.python.versions.Version}
@var version: the version of this program
@type DEFAULT_CONFIG_FILES: C{list} of C{string}
@var DEFAULT_CONFIG_FILES: the default config files to load (in order)
@var DEFAULTS: the default config parameter values for the main program
@var DHT_DEFAULTS: the default config parameter values for the default DHT

"""

import os, sys
from ConfigParser import SafeConfigParser

from twisted.python import log, versions
from twisted.trial import unittest

class ConfigError(Exception):
    """Errors that occur in the loading of configuration variables."""
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

# Set the home parameter
home = os.path.expandvars('${HOME}')
if home == '${HOME}' or not os.path.isdir(home):
    home = os.path.expanduser('~')
    if not os.path.isdir(home):
        home = os.path.abspath(os.path.dirname(sys.argv[0]))

DEFAULT_CONFIG_FILE=home + '/coast/cepConf.conf'

class KConfigParser(SafeConfigParser):
    """Adds 'gettime' and 'getstringlist' to ConfigParser objects.
    
    @ivar time_multipliers: the 'gettime' suffixes and the multipliers needed
        to convert them to seconds
    """
    
    time_multipliers={
        's': 1,    #seconds
        'm': 60,   #minutes
        'h': 3600, #hours
        'd': 86400,#days
        }

    def gettime(self, section, option):
        """Read the config parameter as a time value."""
        mult = 1
        value = self.get(section, option)
        if len(value) == 0:
            raise ConfigError("Configuration parse error: [%s] %s" % (section, option))
        suffix = value[-1].lower()
        if suffix in self.time_multipliers.keys():
            mult = self.time_multipliers[suffix]
            value = value[:-1]
        return int(float(value)*mult)
    
    def getstring(self, section, option):
        """Read the config parameter as a string."""
        return self.get(section,option)
    
    def getstringlist(self, section, option):
        """Read the multi-line config parameter as a list of strings."""
        return self.get(section,option).split()

    def optionxform(self, option):
        """Use all uppercase in the config parameters names."""
        return option.upper()

# Initialize the default config parameters
config = KConfigParser()
config.read(DEFAULT_CONFIG_FILE)
print config.sections()
#config.add_section(config.get('DEFAULT', 'DHT'))
#for k in DHT_DEFAULTS:
#    config.set(config.get('DEFAULT', 'DHT'), k, DHT_DEFAULTS[k])

class TestConfigParser(unittest.TestCase):
    """Unit tests for the config parser."""

    def test_uppercase(self):
        config.set('DEFAULT', 'case_tester', 'foo')
        self.failUnless(config.get('DEFAULT', 'CASE_TESTER') == 'foo')
        self.failUnless(config.get('DEFAULT', 'case_tester') == 'foo')
        config.set('DEFAULT', 'TEST_CASE', 'bar')
        self.failUnless(config.get('DEFAULT', 'TEST_CASE') == 'bar')
        self.failUnless(config.get('DEFAULT', 'test_case') == 'bar')
        config.set('DEFAULT', 'FINAL_test_CASE', 'foobar')
        self.failUnless(config.get('DEFAULT', 'FINAL_TEST_CASE') == 'foobar')
        self.failUnless(config.get('DEFAULT', 'final_test_case') == 'foobar')
        self.failUnless(config.get('DEFAULT', 'FINAL_test_CASE') == 'foobar')
        self.failUnless(config.get('DEFAULT', 'final_TEST_case') == 'foobar')
    
    def test_time(self):
        config.set('DEFAULT', 'time_tester_1', '2d')
        self.failUnless(config.gettime('DEFAULT', 'time_tester_1') == 2*86400)
        config.set('DEFAULT', 'time_tester_2', '3h')
        self.failUnless(config.gettime('DEFAULT', 'time_tester_2') == 3*3600)
        config.set('DEFAULT', 'time_tester_3', '4m')
        self.failUnless(config.gettime('DEFAULT', 'time_tester_3') == 4*60)
        config.set('DEFAULT', 'time_tester_4', '37s')
        self.failUnless(config.gettime('DEFAULT', 'time_tester_4') == 37)
        
    def test_floating_time(self):
        config.set('DEFAULT', 'time_float_tester_1', '2.5d')
        self.failUnless(config.gettime('DEFAULT', 'time_float_tester_1') == int(2.5*86400))
        config.set('DEFAULT', 'time_float_tester_2', '0.5h')
        self.failUnless(config.gettime('DEFAULT', 'time_float_tester_2') == int(0.5*3600))
        config.set('DEFAULT', 'time_float_tester_3', '4.3333m')
        self.failUnless(config.gettime('DEFAULT', 'time_float_tester_3') == int(4.3333*60))
        
    def test_string(self):
        config.set('DEFAULT', 'string_test', 'foobar')
        self.failUnless(type(config.getstring('DEFAULT', 'string_test')) == str)
        self.failUnless(config.getstring('DEFAULT', 'string_test') == 'foobar')

    def test_stringlist(self):
        config.set('DEFAULT', 'stringlist_test', """foo
        bar   
        foobar  """)
        l = config.getstringlist('DEFAULT', 'stringlist_test')
        self.failUnless(type(l) == list)
        self.failUnless(len(l) == 3)
        self.failUnless(l[0] == 'foo')
        self.failUnless(l[1] == 'bar')
        self.failUnless(l[2] == 'foobar')
        
