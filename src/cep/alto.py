"""Author: Francesco Bronzino
Description: Module that implements an alto server locally with a preloded database of addresses
"""

import math
import sys
from twisted.internet.defer import Deferred
from twisted.internet import reactor

DEBUG = True

def toRad(value):
	"""Converts numeric degrees to radians."""
	return value*math.pi/180;

class AltoClient:
	"""Simulate the behaviour of the ALTO server in a dummy way. Needs a preloded database.
	If the address requested is not in the db it will use a default distance.
	"""
	def __init__(self, cFile, DEFDIST = 1):
		"""Arguments:
		cFile -- name of the file used to load the addresses DB
		DEFDIST -- the default distance used in case a requested address is not in the DB
		"""
		self.data = None
		self.altoSimulation = self.dummyOrder
		self.cFile = cFile
		self.setCords()
		self.queries = {}
		self.DEFDIST = DEFDIST
		
	def setCords(self):
		"""Parse the file that contain the hostnames and the coordinates."""
		f = open(self.cFile,'r')
		nodes = []
		for line in f:
			nodes.append(line.split(','))
		f.close()
		self.coords = {}
		for node in nodes:
			node[0] = node[0].rsplit()[0]
			try:
				self.coords[node[0]] = [float(node[2].rsplit()[0]),float(node[3].rsplit()[0])]
				if DEBUG: print "ALTO: new address added:", node[0], self.coords[node[0]][0],self.coords[node[0]][1]
			except:
				if DEBUG: print "ALTO: ERROR while reading address", node
		
	def distance(self, nodeA, nodeB):
		"""Calculate the distance in km between two nodes.
		
		Arguments:
		nodeA -- node A coordinates (array of 2 elements)
		nodeB -- node B coordinates (array of 2 elements)
		
		Return:
		the distance between node A and node B
		"""
		R = 6371; #Radius of the earth in km
		dLat = toRad(nodeB[1]-nodeA[1])
		dLon = toRad(nodeB[0]-nodeA[0]) 
		a = math.sin(dLat/2)*math.sin(dLat/2)+math.cos(toRad(nodeA[1]))*math.cos(toRad(nodeB[1]))*math.sin(dLon/2)*math.sin(dLon/2); 
		c = 2*math.atan2(math.sqrt(a), math.sqrt(1-a)); 
		d = R*c;
		return d
		
	
	def randomOrder(self, df, values):
		"""NOT WORKING!!!
		Simulate ALTO logic by ordering randoml the list of addresses.
		
		Arguments:
		df -- 
		values --
		"""
		values = random.shuffle(values)
		df.callback(values) 
		
	def dummyOrder(self, key, clientAddr, otherAddrs):
		"""Simulate ALTO logic.
		Using the list of known nodes coordinates, calculates the distance between nodes and return the ordered list.
		
		Arguments:
		key -- unique ID of the query
		clientAddr -- reference address for ordering the list of nodes
		otherAddrs -- list of nodes' addresses to order
		"""
		if DEBUG: print 'ALTO: calculating distance between',clientAddr,'and',otherAddrs
		try:
			dists = []
			for addr in otherAddrs:
				if self.coords.has_key(addr) and self.coords.has_key(clientAddr):
					dists.append((addr, self.distance(self.coords[clientAddr], self.coords[addr])))
				else:
					dists.append((addr, self.DEFDIST))
		except Exception:
			if DEBUG: print 'ALTO ERROR: no address', addr,clientAddr,`sys.exc_info()`
		try:
			dists = sorted(dists, key= lambda d: d[1])
			ret = []
			for d in dists:
				ret.append(d)
			if self.queries.has_key(key):
				temp = self.queries[key]
				del self.queries[key]
				reactor.callFromThread(temp.callback, (key,ret))
			else:
				if DEBUG: print "ALTO ERROR: don't have the key:",key
		except Exception:
			if DEBUG: print 'ALTO ERROR: error while ordering or returning',`sys.exc_info()`
		
		
		
	def analyze(self, key, clientAddr, otherAddrs):
		"""Start the simulation of an ALTO query.
		
		Arguments:
		key -- unique ID of the query
		clientAddr -- reference address for ordering the list of nodes
		otherAddrs -- list of nodes' addresses to order
		"""
		if DEBUG: print 'ALTO: start analyzing distances'
		df = Deferred()
		#Qui andra' messa l'interfaccia con ALTO
		self.queries[key] = df
		reactor.callLater(0, self.altoSimulation, key, clientAddr, otherAddrs)
		return df
		
		
class AltoTest:
	def __init__(self):
		self.alto = AltoClient('/home/wontoniii/coast/NodesCoordinates')
	
	def runTest(self):
		addrs = ['133.1.172.60', '133.1.172.58' ,'152.8.109.170','59.163.146.20', '142.104.21.243','128.39.36.27']
		myAddrs = ['133.1.172.60', '133.1.172.58' ,'152.8.109.170','59.163.146.20', '142.104.21.243','0.0.0.0']
		for i in range(len(myAddrs)):
			print 'Loop for node: ',myAddrs[i]
			df = self.alto.analyze(str(i),myAddrs[i], addrs)
			df.addCallback(self.printRes)
			
	def printRes(self, (key, nodes)):
		for val in nodes:
			print val
		print '****************************************'
		 
		
		
		
if __name__ == "__main__":
	t = AltoTest()
	t.runTest()
	reactor.run()
		
		
		
		
