"""Author: Francesco Bronzino
Description: Module used to start an instance of a CEP node that test the main functionalities
and the proper functioning of all the components of the system
"""

#!/usr/bin/env python

from twisted.internet import reactor
from twisted.internet.abstract import isIPAddress
from coastkad.kconf import config as kConf
from coastkad.kDht import KDHT
from cep.conf import config as cepConf
from cep.filesystem import Filesystem
from cep.cepCache import CepCache
from cep.webClient import CepWebClient
from cep.cepCcl import Ccl
from cep.cepCore import CepCore
from cep.rgen import Const, ExpNeg
from cep.contentManager import DBReader
from cep.utils import *
from cep.clientGen import ClientGenerator
from cep.cepInterface import CepInterface
from cep.alto import AltoClient
from cep.dhtInterface import DhtInterface
from cep.statsLogger import StatsLogger
from cep.webServer import CepHTTPServer
from cep.db import DB
from cep.webServer import CepHandler
import os
import random
import time
import socket
import sys
import threading

DEBUG = True
MAXTIME = kConf.getint('KSERVER','MAXTIME')
INITTIME = kConf.getint('KSERVER','INITTIME')
WCPATH = cepConf.getstring('WEB_CLIENT', 'WCPATH')
WSPATH = cepConf.getstring('WEB_SERVER', 'WSPATH')
WSPORT = cepConf.getint('WEB_SERVER', 'WSPORT')
CEPCACHE = cepConf.getstring('DEFAULT', 'CEPCACHE')
USEF = cepConf.getboolean('DEFAULT', 'USEF')
DBFILE = cepConf.getstring('DEFAULT', 'DBFILE')
CACHESIZE = cepConf.getint('DEFAULT', 'CACHESIZE')
CACHEPOLICY = cepConf.getint('DEFAULT', 'CACHEPOLICY')
CDN_US_EAST = cepConf.getstringlist('DEFAULT', 'CDN_US_EAST')
CDN_US_WEST = cepConf.getstringlist('DEFAULT', 'CDN_US_WEST')
CDN_EU = cepConf.getstringlist('DEFAULT', 'CDN_EU')
CDN_AS = cepConf.getstringlist('DEFAULT', 'CDN_AS')
CDNPORT = cepConf.getint('DEFAULT', 'CDNPORT')
CDISTR = cepConf.getint('CLIENT_SIM', 'CDISTR')
CPAR = cepConf.getfloat('CLIENT_SIM', 'CPAR')
CSLOPE = cepConf.getint('CLIENT_SIM', 'CSLOPE')
CKEYS = cepConf.getstring('CLIENT_SIM', 'CKEYS')
LOGGER = cepConf.getstring('DEFAULT', 'LOGGER')
P2PCONC = cepConf.getint('DEFAULT', 'P2PCONC')
MINP2P = cepConf.getint('DEFAULT', 'MINP2P') 
MAXCONC = cepConf.getint('DEFAULT', 'MAXCONC')
P2PTH = cepConf.getint('DEFAULT', 'P2PTH')
ACTP2P = cepConf.getboolean('DEFAULT', 'ACTP2P')
ALTOCOORDS = cepConf.getstring('DEFAULT', 'ALTOCOORDS')
WSFILE = cepConf.getstring('WEB_SERVER', 'WSFILE')
NREQ = cepConf.getint('CLIENT_SIM', 'NREQ')
INITWAIT = cepConf.getint('CLIENT_SIM', 'INITWAIT')
DISTCDN = cepConf.getint('DEFAULT', 'DISTCDN')
CSIZE = cepConf.getint('DEFAULT', 'CSIZE')
CSEED = cepConf.getint('CLIENT_SIM', 'CSEED')
RSEED = cepConf.getint('CLIENT_SIM', 'RSEED')

class CepBuilder:
	def __init__(self):
		self.CSEED = CSEED
		self.RSEED = RSEED	
		if CSEED < 0:
			self.CSEED = None
		if RSEED < 0:
			self.RSEED = None
		self.logger = StatsLogger(LOGGER)
		self.dht = KDHT()
		self.dht.loadConfig(kConf, kConf.get('DEFAULT', 'DHT'))
		self.idht = DhtInterface(dht= self.dht)
		self.t = INITTIME
		self.address = None
		reactor.resolve(socket.gethostname()).addCallbacks(self._gotOwnIP,self._resolveOwnFailed)
		self.filesystem = None
		if USEF:
			self.filesystem = Filesystem(CEPCACHE)
		else:
			self.filesystem = Filesystem(None)
		self.cache = CepCache(DBFILE, CACHESIZE, CACHEPOLICY, self.filesystem)
		self.clientHTTP = CepWebClient(WCPATH)
		self.serverHTTP = CepHTTPServer(('', WSPORT), CepHandler)
		self.serverHTTP.setup(WSPATH, DBFILE, self.logger, filename = WSFILE)
		# Start a thread with the server -- that thread will then start one
		# more thread for each request
		self.server_thread = threading.Thread(target=self.serverHTTP.serve_forever)
		# Exit the server thread when the main thread terminates
		self.server_thread.daemon = True
		self.server_thread.start()
		self.failed = 0
		self.cdn = []
		#Su alto c'e' ancora da lavorarci un secondo
		self.altoClient = AltoClient(ALTOCOORDS)
		self.arrivals = None
		if CDISTR == CONST:
			self.arrivals = Const(CPAR, x = self.CSEED)
		elif CDISTR == EXP:
			self.arrivals = ExpNeg(CPAR, x = self.CSEED)
		self.contents = DBReader(CKEYS, CSLOPE, seed = self.RSEED)
		self.ips = False
		self.joined = False
		self.tout = reactor.callLater(random.randint(200,400),self.timeout)
		
	def timeout(self):
		HOST, PORT = "planetlab1.di.unito.it", 9999
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			sock.connect((HOST, PORT))
			sock.send('ERROR')
		finally:
			sock.close()
			self.stop()
		
	def _gotOwnIP(self, host):
		if DEBUG: print 'CepBuilder: Own addr:',host
		self.address = [host, WSPORT]
		
	def _resolveFailed(self, host, port):
		if DEBUG: print 'CepBuilder: file to discover own address:'
		
	def _resolveOwnFailed(self, host):
		if DEBUG: print 'CepBuilder: file to discover own address:'
		
	def _gotIP(self, host,port):
		if DEBUG: print 'CepBuilder: CDN addr:',host,port
		self.cdn.append((host,port))
		if len(self.cdn) + self.failed == len(CDNADDRS):
			reactor.callLater(0,self.contactAlto)
			
	def contactAlto(self):
		#Indirizzi fittizzi usati per indicare le localita' geografiche al server ALTO
		addrs = ['1.1.1.1','2.2.2.2','3.3.3.3','4.4.4.4']
		try:
			df = self.altoClient.analyze('1', self.address[0], addrs)
		except Exception:
			print 'ERROR: no address', self.address,`sys.exc_info()`
		df.addCallback(self.gotFromAlto)
		
	def gotFromAlto(self, (key,addrs)):
		if addrs[0][0] == '1.1.1.1':
			temp = CDN_US_EAST
		elif addrs[0][0] == '2.2.2.2':
			temp = CDN_US_WEST
		elif addrs[0][0] == '3.3.3.3':
			temp = CDN_EU
		elif addrs[0][0] == '4.4.4.4':
			temp = CDN_AS
		else:
			if DEBUG: print "ERROR: error finding the closest cdn group"
			return
		for node in temp:
			self.cdn.append(((node,CDNPORT),(addrs[0][1]+1)*DISTCDN))
		if DEBUG:
			for node in self.cdn:
				print "CepBuilder: CDN node:",`node`
		self.ips = True
		reactor.callLater(0,self._finishStart)
			
	def _finishStart(self):
		if not self.joined or not self.ips:
			if DEBUG: print 'CepBuilder: still not complete'
			return
		self.ccl = Ccl(self.address, self.cdn, self.altoClient, dht = self.idht, MINP2P = MINP2P, MAXCONC = MAXCONC, P2PTH = P2PTH, ACTP2P = ACTP2P, CSIZE = CSIZE)
		self.ccore = CepCore(self.cache, self.clientHTTP, self.ccl, self.address, self.idht, self.logger)
		self.iCep = CepInterface(self.ccore, self.logger)
		self.clients = ClientGenerator(self.iCep, self.arrivals, self.contents, self.logger, mreq = NREQ)
		self.logger.startLogging(time.time())
		self.tout.cancel()
		if DEBUG: print 'CepBuilder: Everything ready'
		
		try:
			HOST, PORT = "planetlab1.di.unito.it", 9999
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.connect((HOST, PORT))
			sock.send(socket.gethostname())
		except Exception:
			HOST, PORT = "planetlab2.di.unito.it", 9999
			sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			sock.connect((HOST, PORT))
			sock.send(socket.gethostname())
		finally:
			sock.close()
			self.stop()
		
	def getCdn(self):
		if not self.address:
			reactor.callLater(1,self.getCdn)
			return
		if DEBUG: print "Got my own address: ", self.address
		self.contactAlto()
		
	def _nextTime(self):
		"""Next time for trying to connect to the DHT"""
		ret = random.randint(0,self.t)
		if self.t < MAXTIME:
			self.t*2
		return ret
		
	def _joined(self,result):
		"""The node has succesfully joined the network"""
		msg = 'Joined the DHT with this address: '+`result`
		if DEBUG: print msg
		#log.msg(msg)
		self.joined = True
		reactor.callLater(0,self._finishStart)
		reactor.callLater(60*60*3,self.checkJoined)
	
	def _joinError(self,reason):
		"""The node has succesfully joined the network"""
		msg = 'ERROR: Not joined for this reason: '+`reason`
		log.err(msg)
		reactor.callFromThread(self.join)
		
	def join(self):
		"""The node has succesfully joined the network"""
		time.sleep(self._nextTime())
		#log.msg('Start joining the DHT')
		self.df = self.dht.join()
		self.df.addCallbacks(self._joined, self._joinError)
		
	def checkJoined(self):
		if not self.dht.getJoined():
			reactor.callFromThread(self.join)
		else:
			reactor.callLater(60*60*3, self.checkJoined)
		
	def start(self):
		reactor.callLater(0,self.getCdn)
		reactor.callLater(0,self.join)
		reactor.run()
		if DEBUG: print "Shutted down the reactor"
		self.logger.finishLogging(time.time())
		if DEBUG: print "Close the logger file"
		self.serverHTTP.shutdown()
		if DEBUG: print "Shutted down the web server"
		self.server_thread.join(5)
		if DEBUG: print "Joined the web server thread:", self.server_thread.is_alive()
		
	def stop(self):
		#reactor.stop()
		if DEBUG: print 'CepBuilder: finished everything'
		
if __name__=='__main__':
	d = CepBuilder()
	d.start()
