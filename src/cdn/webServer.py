"""
	This module provide http interface provided by the cdn nodes
	Python's modules are extended to offer the funtionalities requeried by the cep architecture
"""


import SimpleHTTPServer
import BaseHTTPServer
import SocketServer
from cdn.db import DB
from cdn.utils import createKey, divKey
import os
import sys
import time
from SocketServer import ThreadingMixIn
import threading
import socket

DEBUG = True

class CdnHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
	def __init__(self, request, client_address, server, other = None):
		"""
			Initialize the handler of the requests
		"""
		SimpleHTTPServer.SimpleHTTPRequestHandler.__init__(self,request, client_address, server)
		
	def do_GET(self):
		"""
			Handle a get request
		"""
		#Devo introdurre quello che voglio prima di chiamare la do_GET della classe padre
		#Questo mi permetterebbe di aggiungere quello che voglio
		#Per il momento uso sempre lo stesso file: cdnTempFile
		ctype = self.guess_type(self.path)
		if DEBUG: print "CdnHandler: ", threading.currentThread().getName()
		try:
			key, chunk = divKey(self.path)
		except Exception:
			self.send_error(404, "File not found")
			print "CdnHandler ERROR: dividing keys",`sys.exc_info()`
			return
		key = key.lstrip('/')
		self.path = self.server.getFilename(key,chunk) 
		if not self.path:
			if DEBUG: 'CdnHandler: File not found in the db'
			self.send_error(404, "File not found")
			return
		self.path = self.server.getServerPath() + self.path
		print self.path
		try:
			#Always read in binary mode. Opening files in text mode may cause
			# newline translations, making the actual size of the content
			# transmitted *less* than the content-length!
			f = open(self.path, 'rb')
		except IOError:
			if DEBUG: print 'CdnHandler ERROR: file not found', self.path, `sys.exc_info()`
			self.send_error(404, "File not found")
		else:
			if DEBUG: print "CdnHandler: requested file:",self.path
			ctype = self.guess_type(self.path)
			self.send_response(200)
			self.send_header("Content-type", ctype)
			fs = os.fstat(f.fileno())
			self.send_header("Content-Length", str(fs[6]))
			self.send_header("Content-Size", self.server.getFileSize())
			#Qui posso aggiungere tutti gli header che voglio
			self.end_headers()
			self.copyfile(f, self.wfile)
			f.close()
			self.close_connection = 1
		
		
#We are not interested in POST requests
#Maybe could implement a do_POST that always return an error


class CdnHTTPServer(ThreadingMixIn, BaseHTTPServer.HTTPServer):
	"""
		Extend a normal python HTTP server. This will let us to manage various stuffs
	"""
	def setup(self, serverPath, db, logger, filename = None, CSIZE = 320):
		#Qui posso aggiungere quello che voglio
		self.serverPath = serverPath
		try:
			os.stat(self.serverPath)
		except:
			os.makedirs(self.serverPath)
		self.db = db
		self.filename = filename
		if filename:
			self.getFilename = self.getConst
		self.CSIZE = CSIZE
		self.logger = logger
		self.request_queue_size = 8192
		#self.daemon_threads = True
		
	def getServerPath(self):
		"""
			return the value of serverPath
		"""
		return self.serverPath
		
	def getFileSize(self):
		return self.CSIZE
		
	def getFilename(self, key, chunk):
		"""
			Return the filename corresponding to the key
		"""
		#Mettiamo i dati in un database, in modo da non dover avere una corrispondenza diretta tra chiave e nome del file
		l = self.db.retrieveValue(key)
		if not l:
			if DEBUG: print 'cdnHTTPServer: key',key,' not found'
			return None
		if DEBUG: print 'cdnHTTPServer:',key,'-->',l[1]
		f = createKey(l[1],chunk)
		t = time.time()
		self.logger.logCDNChunk(t)
		return f
		
		
	def getConst(self,key, chunk):
		"""
			Return the filename corresponding to the key, but return always the same file
			Only for tests purposes
		"""
		t = time.time()
		self.logger.logCDNChunk(t)
		return self.filename
		
		
		
		
		
def loopResources(f, time, reactor, threading):
	print >> f, "USAGE REPORT AT TIME: ", time.time()
	print >> f, len(threading.enumerate()), "\n\n"
	reactor.callLater(1,loopResources, f, time, reactor, threading)
		
		
if __name__== "__main__":
	#db = DB('/home/wontoniii/coast/tmp/cdncache/cache.db')
	import resource
	from twisted.internet import reactor
	import time
	import threading
	import socket
	print socket.SOMAXCONN
	f = open('res.txt', 'w')
	df = reactor.callLater(1,loopResources, f, time, reactor, threading)
	server_address = ('', int(sys.argv[1]))
	from cdn.statsLogger import StatsLogger
	logger = StatsLogger('prova.txt')
	logger.startLogging(time.time())
	httpd = CdnHTTPServer(server_address, CdnHandler)
	httpd.setup('./',None, logger, 'cdnTempFile')
	server_thread = threading.Thread(target=httpd.serve_forever)
	server_thread.daemon = True
	server_thread.start()
	rf = reactor.run()
	server_thread.exit()
	f.close()
