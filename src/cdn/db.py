"""Database used by a cdn node"""

import time
from pysqlite2 import dbapi2 as sqlite
from binascii import a2b_base64, b2a_base64
import os

from twisted.trial import unittest

class DBExcept(Exception):
	pass

class khash(str):
	"""Dummy class to convert all hashes to base64 for storing in the DB."""
	
class dht_value(str):
	"""Dummy class to convert all DHT values to base64 for storing in the DB."""

# Initialize the database to work with 'khash' objects (binary strings)
sqlite.register_adapter(khash, b2a_base64)
sqlite.register_converter("KHASH", a2b_base64)
sqlite.register_converter("khash", a2b_base64)

# Initialize the database to work with DHT values (binary strings)
sqlite.register_adapter(dht_value, b2a_base64)
sqlite.register_converter("DHT_VALUE", a2b_base64)
sqlite.register_converter("dht_value", a2b_base64)

class DB:
	"""An sqlite database for storing the cache value in the DB.
	
	@type db: C{string}
	@ivar db: the database file to use
	@type conn: L{pysqlite2.dbapi2.Connection}
	@ivar conn: an open connection to the sqlite database
	"""
	
	def __init__(self, db):
		"""Load or create the database file.
		
		@type db: C{string}
		@param db: the database file to use
		"""
		self.db = db
		try:
			os.stat(db)
		except OSError:
			self._createNewDB(db)
		else:
			self._loadDB(db)
		if sqlite.version_info < (2, 1):
			sqlite.register_converter("TEXT", str)
			sqlite.register_converter("text", str)
		else:
			self.conn.text_factory = str

	def _loadDB(self, db):
		"""Open a new connection to the existing database file"""
		try:
			self.conn = sqlite.connect(database=db, detect_types=sqlite.PARSE_DECLTYPES)
		except:
			import traceback
			raise DBExcept, "Couldn't open DB", traceback.format_exc()
		
	def _createNewDB(self, db):
		"""Open a connection to a new database and create the necessary tables."""
		self.conn = sqlite.connect(database=db, detect_types=sqlite.PARSE_DECLTYPES)
		c = self.conn.cursor()
		#Valori in una tupla:
		#	-key: chiave univoca che identifica un contenuto
		#	-size: la dimensione del del file che si a a recuperare
		#	-path: filename base del file a cui aggiungere il numero del chunk
		c.execute("CREATE TABLE kv (key KHASH, size INTEGER, path TEXT, PRIMARY KEY (key))")
		c.execute("CREATE INDEX kv_key ON kv(key)")
		self.conn.commit()

	def close(self):
		self.conn.close()
		
	def retrieveKeys(self):
		"""Retrieve all keys in the database from the database."""
		c = self.conn.cursor()
		c.execute("SELECT key FROM kv")
		l = []
		rows = c.fetchall()
		for row in rows:
			l.append(row[0])
		return l

	def retrieveValue(self, key):
		"""Retrieve values from the database."""
		c = self.conn.cursor()
		c.execute("SELECT size,path FROM kv WHERE key = ?", (khash(key),))
		row = c.fetchall()
		if row:
			return row.pop()
		return None
	
	def storeValue(self, key, size, path):
		"""Store or update a key and value."""
		#Volendo si potrebbero fare dei controlli sui dati dati
		c = self.conn.cursor()
		c.execute("INSERT OR REPLACE INTO kv VALUES (?, ?, ?)", 
				  (khash(key), size, path))
		self.conn.commit()
		
	def deleteValue(self,key):
		"""Delete a key from the db. Once deleted return the amount of space that was occupied by the cached object"""
		l = self.retrieveValue(key)
		c = self.conn.cursor()
		c.execute("DELETE FROM kv WHERE key = ?", (khash(key),))
		self.conn.commit()
		return l
		
	def clearCache(self):
		"""Delete a key from the db. Once deleted return the amount of space that was occupied by the cached object"""
		c = self.conn.cursor()
		c.execute("DELETE FROM kv")
		self.conn.commit()
		
	def countValues(self):
		"""
			Return the number of values contained in the db
		"""
		c = self.conn.cursor()
		c.execute("SELECT COUNT(key) FROM kv")
		row = c.fetchall()
		return row
