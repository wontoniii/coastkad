"""
	Module of utilities for the cep system
"""

import random

CDN = 1
HTTP = 2
P2P = 3

CONST = 1
EXP = 2

RTIME = '1'
FTIME = '2'
NCONTS = '3'
HTTPC = '4'
P2PC = '5'
CDNT = '6'
P2PT = '7'
DT = '8'
CDNFCT = '9'
P2PFCT = '10'
INITT = '11'
FL = '12'
CACHEFCT = '13'
CACHEC = '14'
CDNC = '15'
CEPC = '16'
VINIT = '17'
CCTO = '18'
CETO = '19'
CC404 = '20'
CE404 = '21'

def newID():
	"""
		Generate a random ID of 20 bits and transform it in a string => 160 bits
	"""
	ret = str(random.getrandbits(20))
	return ret

def createKey(key, chunk):
	"""
		Create a key meshing a key(string) and a chunk number(int)
	"""
	#if chunk.isint
	chunk = str(chunk)
	return key+'.'+chunk
	
def divKey(key):
	"""
		Divide a string generated with the createKey function
	"""
	temp = key.rsplit('.',1)
	temp[1] = int(temp[1])
	return (temp[0],temp[1])
	
def chunksFromSize(size):
	"""
		Return the number of chunks given a file size
	"""
	
class Bitmap:
	"""
		Class that implements a bitmap used to analyze the chunks of a content
	"""
	def __init__(self, m = 0, s = 0):
		"""
			Generate the bitmap using a map and a size
		"""
		self.init = 0
		self.tot = 0
		if not m and s:
			self.size = s
			self.tot = s
			self.init = s
			self.map = 0
			for i in range(s):
				self.map += 1 << i
		else:
			self.map = m
			if s == 0:
				temp = 0
				i = 0
				lock = False
				while temp != m:
					if self.isOne(i):
						temp += 1 << i
						self.tot += 1
						if not lock:
							self.init += 1
					else:
						lock = True
					i += 1
				self.size = i
			else:
				self.size = s
			
	def editFromPerc(self, init, tot, size, conc):
		"""
			init: Number of contiguous chunks from the first one
			tot: Number of total chunks
			size: Total size of the map
			conc: Number of concurrent requests
		"""
		#Da migliorare
		self.map = 0
		self.init = init
		self.tot = tot
		self.size = size
		for i in range(init):
			self.setBit(i)
		for i in random.sample(range(size-init),tot-init):
			n = init+1+i
			if n < self.size:
				self.setBit(i)
	def getTot(self):
		return self.tot
	
	def getInit(self):
		return self.init
	
	def getMap(self):
		return self.map
	
	def setMap(self, m):
		self.map = m
		
	def getSize(self):
		return self.size
		
	def setSize(self, s):
		self.size = s
		
	def getPerc(self):
		temp = 0
		for i in range(self.size):
			if self.isOne(i):
				temp += 1
		return (float(temp)/self.size)*100
				
		
	def isComplete(self):
		for i in range(self.size):
			if not self.isOne(i):
				return False
		return True

	def isOne(self, offset):
		"""
			isOne() returns a nonzero result, 2**offset, if the bit at 'offset' is one.
		"""
		mask = 1 << offset
		return (self.map & mask)
	
	def setBit(self, offset):
		"""
			setBit() returns an integer with the bit at 'offset' set to 1.
		"""
		if offset == self.init:
			self.init += 1
		self.tot += 1
		mask = 1 << offset
		self.map = self.map | mask
		if offset > self.size:
			self.size = offset
	
	def clearBit(self, offset):
		"""
			clearBit() returns an integer with the bit at 'offset' cleared.
		"""
		self.tot -= 1
		if offset < self.init:
			self.init = offset
		mask = ~(1 << offset)
		self.map = self.map & mask

	def toggleBit(self, int_type, offset):
		"""
			toggleBit() returns an integer with the bit at 'offset' inverted, 0 -> 1 and 1 -> 0.
		"""
		mask = 1 << offset
		self.map = self.map ^ mask
		
	def diff(self, m):
		"""
			Calculate the difference bitmap with m
		"""
		return (self.map & m)
	
	def mapToArray(self):
		"""
			Generate a list of integers represing the positions of the map with a 1.
		"""
		arr = []
		for i in range(self.size):
			if self.isOne(i):
				arr.append(i)
		return arr
		
	def toString(self):
		return 'Size: '+str(self.size)+' Init: '+str(self.init)+' Tot: '+str(self.tot)
		
def divFlag(flag):
	"""
		Analyze and divide the flags used to store information in the COAST dht
	"""
	seeder = 1 & flag
	flag = flag >> 1
	t = 2047
	init = flag & t
	flag = flag >> 11
	tot = flag & t
	return (seeder,init,tot)

def genFlag(s,i,t):
	"""
		Given the necessary information generate the flag used to store that information in the COAST dht
	"""
	t = t << 12
	i = i << 1
	return s+i+t
		
class test:
	def __init__(self):
		from coastkad.khash import newID
		self.key = newID()
		
	def startTest(self):
		print 'key',`self.key`
		nkey = createKey(self.key,0)
		print 'new key', `nkey`
		dkey = divKey(nkey)
		print 'div key', `dkey`
		print 'flag',8388607,'is',divFlag(8388607)
		print 1,2047,2047,'is',genFlag(1,2047,2047)
		print 'flag',0,'is',divFlag(0)
		print 0,0,0,'is',genFlag(0,0,0)
		print 'flag',10,'is',divFlag(10)
		print 0,10,10,'is',genFlag(0,10,10)
			
if __name__=="__main__":
	t = test()
	t.startTest()
	print 'Test completed'
