"""The CDN implementation for coast tests.

These modules implement a modified Khashmir, which is a kademlia-like
Distributed Hash Table available at::

  http://khashmir.sourceforge.net/

Great part of the code has been taken from:

  http://www.camrdale.org/apt-p2p/

"""

