""" Modulo per la generazione e la lettura di un database chiave-valore(DHT)
Includere le classi DbGenerator e DbReader per usare le singole funzioni o eseguire da linea di comando
"""

import random
from rgen import Zipf, Uniform, Normal48
from coastkad.db import DB
from coastkad.khash import newID
from util import _normKey
import os,sys
from twisted.internet import reactor


class DbGenerator:
	"""Generate a database for the DHT using a zipf distribution for the popularity of a key"""
	
	#For the moment it uses an uniform generetor between [1,10)
	def __init__(self, n = 1000, pdistr = 1, db = 'khashmir.db'):
		self.db = DB(db)
		self.n = n
		self.prev = None
		#self.r = ZipF(random.randint(0,sys.maxint))
		if pdistr == 1:
			self.r = ZipF()
		elif pdistr == 2:
			self.r = Uniform((1,10))
		elif pdistr == 3:
			self.mean = 50
			self.variance = 1
			self.r = Normal48((self.mean,self.variance))
		
	def genKey(self):
		return _normKey(newID())
		
	def _genValue(self, popularity):
		"""
		Generate a value to insert in the db.
		Each value is a 10 bytes string:
			-4 bytes for the IP address
			-2 bytes for the port
			-4 bytes for the flags
		"""
		return os.urandom(10)
	
	def genValue(self):
		#Determinare la popolarita e generare un numero di valori relativo
		popularity = int(self.r.random())
		val = []
		for i in range(popularity):
			val.append(self._genValue(popularity))
		return val
		
	def genDb(self):
		for i in range(self.n):
			key = self.genKey()
			val = self.genValue()
			for v in val:
				self.db.storeValue(key,v)
			
	def tearDown(self):
		self.db.close()
			
class DbReader:
	"""Read a database"""
	#At the moment it reads all the keys stored in the db and iterates the array generated.
	#Should work with db index...
	
	def __init__(self, n = None, db = 'khashmir.db'):
		self.db = DB(db)
		self.keys = self.db.retrieveKeys()
		print len(self.keys)
		self.ind = 0
		self.n = n
		if not n:
			self.n = len(self.keys)
		
	def getNext(self):
		"""Get the values associated to the next key"""
		if self.ind == self.n:
			return None
		ret = {}
		print self.ind
		ret['key'] = self.keys[self.ind]
		ret['value'] = self.db.retrieveValues(self.keys[self.ind])
		self.ind += 1
		return ret
		
	def tearDown(self):
		self.db.close()
		
class DbReaderV(DbReader):
	"""Read a database one value at a time"""
	#At the moment it reads all the keys stored in the db and iterates the array generated.
	#Should work with db index...
	
	def __init__(self, n = None, db = 'khashmir.db'):
		DbReader.__init__(self,n,db)
		self.v = []
		
	def getNext(self):
		"""Get the values associated to the next key"""
		if len(self.v) == 0:
			if self.ind == self.n:
				return None
			self.v = self.db.retrieveValues(self.keys[self.ind])
			self.ind = self.ind+1
		ret = {}
		ret['key'] = self.keys[self.ind - 1]
		ret['value'] = self.v.pop()
		self.ind = self.ind+1
		return ret
		
class DbReaderStore:
	"""Read/write a database"""
	from twisted.internet import reactor
	
	def __init__(self, n = None, db = 'khashmir.db'):
		self.db = DB(db)
		self.keys = self.db.retrieveKeys()
		self.r = Uniform((1,9))
		reactor.callLater(10,self.update)
		
	def update(self):
		self.keys = self.db.retrieveKeys()
		reactor.callLater(10,self.update)
		
	def store(self,key,value):
		self.db.storeValue(key,value)
		
	def genKey(self):
		return _normKey(newID())
		
	def _genValue(self, popularity):
		return os.urandom(150*popularity)
	
	def genValue(self):
		#Determinare la popolarita e generare un numero di valori relativo
		popularity = self.r.random()
		val = self._genValue(popularity)
		return val
		
	def getNext(self, op):
		"""Get the values associated to the next key"""
		ret = {}
		if op == 2:
			n = random.randint(0, len(self.keys)-1)
			self.v = self.db.retrieveValues(self.keys[n])
			ret['key'] = self.keys[n]
			ret['value'] = self.v.pop()
		elif op == 1:
			ret['key'] = self.genKey()
			ret['value'] = self.genValue()
		return ret
		
class DbManagerZipf:
	def __init__(self, n = 5000, alpha = 1):
		self.keys = []
		for i in range(n):
			self.keys.append(self.genKey())
		self.zipf = Zipf((alpha, n))
		
	def genKey(self):
		return _normKey(newID())
		
	def genValue(self):
		return os.urandom(150)
		
	def getNext(self):
		"""Get the values associated to the next key"""
		ret = {}
		n = self.zipf.randZRank()
		v = self.genValue()
		ret['key'] = self.keys[n]
		ret['value'] = v
		return ret
		
		
class Run_dbManager:
	def __init__(self, op = 1, n = 1000, db = 'khashmir.db'):
		"""
		@param op: the operation executed by the manager. 1 for generation, 2 for printing to standard output
		@param db: filename for the file to be created
		@param n: the number of keys to be generated or read
		"""
		self.dbm = None
		self.op = op
		#1 is for generator, 2 for reader
		if op == 1:
			self.dbm = DbGenerator(n = n, pdistr = 3, db = db)
		elif op == 2:
			self.dbm = DbReaderV(n = n, db = db)
		
	def run(self):
		if self.op == 1:
			print "Generating the db"
			self.dbm.genDb()
			print "Db generated"
			self.dbm.tearDown()
		elif self.op == 2:
			print "Reading the db"
			next = self.dbm.getNext()
			key = next['key']
			i = 1
			while next:
				print 'key',next['key'],'value',next['value']
				next = self.dbm.getNext()
				i += 1
			print "All db read"
			self.dbm.tearDown()
		
if __name__ == "__main__":
	#t = Run_dbManager(int(sys.argv[1]), int(sys.argv[2]), sys.argv[3])
	#t.run()
	t = DbManagerZipf(alpha = 1, n = 5000)
	for i in range(100):
		print t.getNext()
			
