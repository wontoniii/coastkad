from coastkad.bencode import bencode, bdecode
from coastkad.khash import newID
from twisted.internet import reactor
from twisted.internet.protocol import ClientFactory, Protocol
from twisted.python import log
import random
from coastkad.kconf import config
from coastkad import rgen
import hashlib
from dbManager import DbReader, DbGenerator, DbReaderStore, DbReaderV
from coastkad.clientGen import ClientGen

import time

import sys

#Parametric options
DEBUG = config.getboolean('DEFAULT','DEBUG')
TOUT = config.getint('KCLIENT','TOUT')
CTOUT = config.getint('KCLIENT','CTOUT')
NKEYS = config.getint('KCLIENT','NKEYS')
DBCLIENT = config.getstring('KCLIENT','DBCLIENT')
OTNODES = config.getint('KCLIENT','OTNODES')
SHUTDOWNTIME = config.getint('KCLIENT','SHUTDOWNTIME')

#Protocol commands
STORE = 's'
GET = 'g'
TON = 't'
TOFF = 'f'
OK = 'o'
NOK = 'n'
PING = 'p'
GS = 'm'
GSTS = 'a'

class KClientProtocolTCP(Protocol):
	def __init__(self, client):
		self.client = client
	
	def connectionMade(self):
		if DEBUG: print 'Handshake completed'
		self.client.clientConnectionMade()
		
	def dataReceived(self, data):
		if DEBUG: print 'Packet received: ', `data`
		self.client.gotResponse(data)
		
	def sendMessage(self, req):
		if DEBUG: print 'Sending this request: ', `req`
		self.transport.write(req)
		
	def closeConnection(self):
		if DEBUG: print 'Closing connection'
		
class KClientFactory(ClientFactory):
	def __init__(self,node,client):
		self.connected = False
		self.failed = False
		self.node = node
		self.client = client
		
	def startedConnecting(self, connector):
		msg = 'Connection has started with: '+`self.node`
		if DEBUG: print msg
		log.msg(msg)
		
	def clientConnectionMade(self):
		msg = 'Connected to: '+`self.node`
		if DEBUG: print msg
		#log.msg(msg)
		self.connected = True
		self.failed = False
		reactor.callFromThread(self.client.connectionMade,self.node)
		
	def clientConnectionFailed(self, connector, reason):
		msg = 'Connection with '+`connector.getDestination()`+'has failed because of: '+`reason`
		if DEBUG: print msg
		#log.err(msg)
		self.failed = True
		self.connected = False
		reactor.callFromThread(self.client.connectionError,self.node,reason)
		
	def clientConnectionLost(self, connector, reason):
		msg = 'Connection with '+`connector.getDestination()`+'has been lost because of: '+`reason`
		if DEBUG: print msg
		#log.err(msg)
		self.failed = True
		self.connected = False
		reactor.callFromThread(self.client.connectionError,self.node,reason)
	
	def clientConnectionClose(self):
		msg = 'Connection with ... is getting closed'
		if DEBUG: print msg
		self.connected = False
		#reactor.callFromThread
		
	def buildProtocol(self, addr):
		self.protocol = KClientProtocolTCP(self)
		return self.protocol
		
	def gotResponse(self, msg):
		"""Store a received response in a file"""
		mtime = time.time()
		rep = bdecode(msg)
		rep['time'] = mtime
		if DEBUG: print rep
		reactor.callFromThread(self.client.gotReply,[self.node,rep])
		
	def isConnected(self):
		return self.connected
		
	def hasFailed(self):
		return self.failed
		
	def sendRequest(self,req):
		self.protocol.sendMessage(bencode(req))
		return time.time()

class KClient():
	def __init__(self,filename):
		self.filename = filename
		self.connections = {}
		
	def connectionError(self,node,reason):
		log.err("Connection with: "+`node`+" has been lost for this reason: "+`reason`)
		
	def connectionMade(self, node,reason):
		log.msg("Connection with: "+`node`+" has been established")
		
	def getNodes(self):
		"""Select all dht nodes from file"""
		f = open(self.filename,'r')
		self.nodes = []
		for line in f:
			self.nodes.append(line.rstrip())
		if DEBUG: print self.nodes
		f.close()
		
	def selectNode(self):
		"""Select randomly a node from his list of nodes"""
		return self.nodes[random.randint(0, len(self.nodes)-1)]

	def start(self):
		"""Initialize all the connections and start the main loop"""
		self.getNodes()
		for node in self.nodes:
			self.connections[node] = KClientFactory(node, self)
			reactor.connectTCP(node, config.getint('KSERVER','PORT'), self.connections[node])
		reactor.callFromThread(self.mainLoop)
		
	def getNext(self, method = None):
		"""Get the next random number generated from the random generator instance, using the method passed as argument"""
			
	def mainLoop(self):
		"""The main loop of the client, called by the start function"""
		
	def gotReply(self,result):
		logmsg = "Got reply: "+`result[1]`+"\nfrom node: "+`result[0]`+"\nrequest: "+`self.sent[result[0]][result[1]['mid']]['value']`+"\n\n"
		if result[1]['cmd'] == OK:
			log.msg(logmsg)
		else:
			log.err(logmsg)
			
class KClientAut(KClient):
	#The problem filedescriptor out of range in select() should be solved. Check with a long run
	def __init__(self,filename):
		KClient.__init__(self,filename)
		self.connecting = {}
		self.port = {}
		self.queued = {}
		self.sent = {}
		self.cTimeout = {}
		self.reader = ClientGen()
		
	def start(self):
		self.getNodes()
		reactor.callFromThread(self.mainLoop)
		
	def stop(self):
		if DEBUG: print 'Just waited some time to let nodes to reply'
		if len(self.sent) > 0:
			reactor.callLater(10,self.stop)
			return
		log.msg("Finished")
		reactor.stop()
		
	def timeout(self,node):
		n = 0
		if self.connecting.has_key(node):
			del self.connecting[node]
		if self.sent.has_key(node):
			n = len(self.sent[node])
			if n > 0:
				log.err('ERROR: node '+`node`+' has not replied to '+`n`+' requests')
			del self.sent[node]
		if self.port.has_key(node):
		#Vedere se usare un altro invece di disconnect
			self.port[node].disconnect()
			del self.port[node]
		if self.queued.has_key(node):
			del self.queued[node]
		if self.connections.has_key(node):
			self.connections[node].stopFactory()
			del self.connections[node]
		log.err("Connection with: "+`node`+" timed out")
	
	def connTimeout(self,node):
		n = 0
		if self.connecting.has_key(node):
			del self.connecting[node]
		if self.sent.has_key(node):
			n = len(self.sent[node])
			if n > 0:
				log.err('ERROR: node '+`node`+' has not replied to '+`n`+' requests')
			del self.sent[node]
		if self.cTimeout.has_key(node):
			del self.cTimeout[node]
		if self.port.has_key(node):
			self.port[node].disconnect()
			del self.port[node]
		if self.queued.has_key(node):
			del self.queued[node]
		if self.connections.has_key(node):
			self.connections[node].stopFactory()
			del self.connections[node]
		log.msg("Connection with: "+`node`+" has expired")
		
	def connectionError(self,node,reason):
		n = 0
		if self.connecting.has_key(node):
			try:
				self.connecting[node].cancel()
			except:
				pass
			del self.connecting[node]
		if self.sent.has_key(node):
			n = len(self.sent[node])
			if n > 0:
				log.err('ERROR: node '+`node`+' has not replied to '+`n`+' requests')
			del self.sent[node]
		if self.cTimeout.has_key(node):
			del self.cTimeout[node]
		if self.connections.has_key(node):
			self.connections[node].stopFactory()
			del self.connections[node]
		log.err("Connection with: "+`node`+" has been lost for this reason: "+`reason`)
		
	def connectionMade(self, node):
		try:
			self.connecting[node].cancel()
		except:
			pass
		del self.connecting[node]
		self.cTimeout[node] = reactor.callLater(CTOUT,self.timeout,node)
		log.msg("Connection with: "+`node`+" has been established")
		reactor.callFromThread(self.flush,node)
		
	def gotReply(self,result):
		#Prepare the log message with:
		#	_the reply received
		#	_the node hostname
		#	_the original request
		sent = self.sent[result[0]].pop(result[1]['mid'])
		logmsg = "Got reply: "+`result[1]`+"\nfrom node: "+`result[0]`+"\nrequest: "+`sent`+"\n\n"
		#Store the message in the error log in case it is a NOK reply
		if result[1]['cmd'] == OK:
			log.msg(logmsg)
		else:
			log.err(logmsg)
		self.cTimeout[result[0]].reset(CTOUT)
		reactor.callFromThread(self.storeStats, sent, result[1])
		
	def storeStats(self, sent, rec):
		if sent['cmd'] == GET:
			for value in sent['value']:
				#log.err('NOT ERROR '+`value`)
				if value not in rec['value'][0]:
					log.err(`sent['mid']`+' CONSISTENCY ERROR')
					return
		if SELNEXT == 1 and sent['cmd'] == STORE:
			self.reader.store(sent['key'],sent['value'])
		log.msg(`sent['mid']`+' STATS: '+str(rec['time']-sent['time']))
		
	def selectNode(self):
		"""Select randomly a node from his list of nodes"""
		return self.nodes[random.randint(0, len(self.nodes)-1)]
		
	def getMid(self):
		return random.randint(0,56000)
		
	def sendPing(self,node):
		msg = {'mid': self.getMid(),'cmd':PING,'value':'ping'}
		self.connections[node].sendRequest(msg)
	
	def prepareMessage(self, node, msg):
		if not self.connections.has_key(node):
			self.startConnecting(node)
			self.queued[node].append(msg)
		elif self.connecting.has_key(node):
			self.queued[node].append(msg)
		else:
			self.queued[node].append(msg)
			reactor.callFromThread(self.flush,node)
	
	def sendStore(self, val):
		node = self.selectNode()
		msg = {'mid':self.getMid(),'cmd':STORE,'value':{'key':val['key'],'value':val['value']}}
		self.prepareMessage(node, msg)
		self.sent[node][msg['mid']] = {}
		self.sent[node][msg['mid']]['cmd'] = STORE
		self.sent[node][msg['mid']]['mid'] = msg['mid']
		self.sent[node][msg['mid']]['key'] = val['key']
		self.sent[node][msg['mid']]['value'] = val['value']
	
	def sendGet(self, val):
		node = self.selectNode()
		msg = {'mid':self.getMid(),'cmd':GET,'value':val['key']}
		self.prepareMessage(node, msg)
		self.sent[node][msg['mid']] = {}
		self.sent[node][msg['mid']]['cmd'] = GET
		self.sent[node][msg['mid']]['mid'] = msg['mid']
		self.sent[node][msg['mid']]['key'] = val['key']
		self.sent[node][msg['mid']]['value'] = val['value']
		
		
	def startConnecting(self,node):
		self.connections[node] = KClientFactory(node, self)
		self.port[node] = reactor.connectTCP(node, config.getint('KSERVER','PORT'), self.connections[node])
		self.connecting[node] = reactor.callLater(TOUT,self.timeout,node)
		self.sent[node] = {}
		self.queued[node] = []
		
	def flush(self,node):
		while len(self.queued[node])>0:
			msg = self.queued[node].pop()
			log.msg('Sending a request to node: '+`node`)
			self.sent[node][msg['mid']]['time'] = self.connections[node].sendRequest(msg)
			
	def mainLoop(self):
		op = self.reader.getNextOp()
		ret = self.reader.getNextVal()
		if not ret:
			self.stop()
			return
		if op == 1:
			self.sendStore(ret)
		elif op == 2:
			self.sendGet(ret)
		reactor.callLater(self.reader.getNextTime(),self.mainLoop)
		
#This class still need to be updated		
class KClientInt(KClient):
	"""A client that get instructions from standard input"""
	
	def __init__(self,filename):
		#self.filename = filename
		#self.connections = {}
		KClient.__init__(self,filename)
		
	def selectNode(self):
		"""Select a node from standard input and check that belong to his list of nodes"""
		ret = None
		while not ret:
			inp = raw_input('With which node do you want to interact: ')
			if inp in self.nodes:
				ret = inp
		return ret
		
	def selectOption(self):
		ret = 'z'
		val = ['a','b','c','d']
		print("a)Get\nb)Store\nc)ping\nd)Do nothing")
		while ret not in val:
			ret = raw_input('Option: ')
		return ret
		
	def sendPing(self,node):
		msg = {'mid':1,'cmd':PING,'value':'ping'}
		self.connections[node].sendRequest(msg)
	
	def sendStore(self,node):
		msg = {'mid':random.randint(0,56000),'cmd':STORE,'value':{'key':raw_input('Key: '),'value':raw_input('Value: ')}}
		self.connections[node].sendRequest(msg)
	
	def sendGet(self,node):
		msg = {'mid':newID(),'cmd':GET,'value':raw_input('Key: ')}
		self.connections[node].sendRequest(msg)
			
	def mainLoop(self):
		sel = self.selectOption()
		node = None
		if sel == 'd':
			pass
		else:
			node=self.selectNode()
			if sel == 'a' and self.connections[node].isConnected():
				self.sendGet(node)
			elif sel == 'b' and self.connections[node].isConnected():
				self.sendStore(node)
			elif sel == 'c' and self.connections[node].isConnected():
				self.sendPing(node)
			else:
				print 'Node:', node, 'not connected'
		reactor.callLater(0.1,self.mainLoop)
		
class KClientOT(KClient):
	"""A client created to offer an extensible base for one time calling clients"""
	
	def __init__(self,filename):
		KClient.__init__(self,filename)
		self.port = {}
		self.connecting = {}
		self.sent = {}
		self.nAlive = []
		self.completed = False
		
	def selectNode(self):
		node = self.nodes.pop()
		return node
		
	def timeout(self,node):
		if self.connecting.has_key(node):
			del self.connecting[node]
		if self.port.has_key(node):
			self.port[node].disconnect()
			del self.port[node]
		if self.connections.has_key(node):
			self.connections[node].stopFactory()
			del self.connections[node]
		log.err("Connection with: "+`node`+" timed out")
		if not self.completed:
			self.completed = self.startConnecting()
		
	def connectionError(self,node,reason):
		if self.connecting.has_key(node):
			try:
				self.connecting[node].cancel()
			except:
				pass
			del self.connecting[node]
		if self.port.has_key(node):
			del self.port[node]
		if self.connections.has_key(node):
			self.connections[node].stopFactory()
			del self.connections[node]
		log.err("Connection with: "+`node`+" has been lost for this reason: "+`reason`)
		if not self.completed:
			self.completed = self.startConnecting()
		
	def startConnecting(self):
		try:
			node = self.selectNode()
		except IndexError:
			self.complete()
			return True
		else:
			self.connections[node] = KClientFactory(node, self)
			self.port[node]=reactor.connectTCP(node, config.getint('KSERVER','PORT'), self.connections[node])
			self.connecting[node] = reactor.callLater(TOUT,self.timeout,node)
			return False
		
	def start(self):
		"""Initialize all the connections and start the main loop"""
		self.getNodes()
		if DEBUG: print 'Got all hostnames'
		for i in range(OTNODES):
			if not self.completed:
				self.completed = self.startConnecting()
		
	def connectionMade(self, node):
		"""Send the unique request to the node who has been just connected"""
		self.nAlive.append(node)
		
	def gotReply(self,rep):
		if rep[1]['cmd'] == OK:
			log.msg("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
			self.nAlive.append(rep[0])
		else:
			log.err("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		self.port[rep[0]].disconnect()
		self.connections[rep[0]].stopFactory()
		del self.connections[rep[0]]
		if not self.completed:
			self.completed = self.startConnecting()
		
	def complete(self):
		if DEBUG: print 'Just waited some time to let nodes to reply'
		if len(self.connections):
			reactor.callLater(SHUTDOWNTIME,self.complete)
			return
		f = open(self.filename, 'w')
		for node in self.nAlive:
			print >>f,node
		f.close()
		try:
			reactor.stop()
		except:
			if DEBUG: print 'Reactor already stopped'
			
class KClientStart(KClientOT):
	"""A client that send a turn on message to all the nodes that are writed in a file"""

	def connectionMade(self, node):
		self.connecting[node].cancel()
		msg = {'mid':1,'cmd':TON,'value':'start'}
		self.connections[node].sendRequest(msg)			
			
class KClientStop(KClientOT):
	"""A client that send a turn off message to all the nodes that are writed in a file""" 
	
	def connectionMade(self, node):
		self.connecting[node].cancel()
		msg = {'mid':1,'cmd':TOFF,'value':'stop'}
		self.connections[node].sendRequest(msg)
			
class KClientStatusCheck(KClientOT):
	"""A client that contact every node that is up and request his status, logging it in a log file"""
	
	def __init__(self,filename, filename2):
		KClientOT.__init__(self,filename)
		self.f2 = open(filename2,'w')
		self.actives = 0
		self.notJoined = 0
		self.nnodes = 0
		
	def getNodes(self):
		"""Select all dht nodes from file"""
		KClientOT.getNodes(self)
		self.nnodes = len(self.nodes)
	
	def connectionMade(self, node):
		self.connecting[node].reset(TOUT)
		msg = {'mid':1,'cmd':GS,'value':'Give me your stats'}
		self.connections[node].sendRequest(msg)
		
	def gotReply(self,rep):
		self.actives = self.actives + 1
		if rep[1]['cmd'] == OK:
			log.msg("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		else:
			log.err("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		if not rep[1]['value']['joined']:
			self.notJoined = self.notJoined + 1
		else:
			self.nAlive.append(rep[0])
		self.f2.write(rep[0]+str(rep[1]['value']['joined'])+','+rep[1]['value']['id']+','+`rep[1]['value']['addr'][0]`+'\n')
		self.port[rep[0]].disconnect()
		self.connections[rep[0]].stopFactory()
		del self.connections[rep[0]]
		if not self.completed:
			self.completed = self.startConnecting()
		
	def complete(self):
		if DEBUG: print 'Just waited some time to let nodes to reply'
		if len(self.connections):
			reactor.callLater(SHUTDOWNTIME,self.complete)
			return
		log.msg('Number of active nodes: '+`self.actives`+'\nNumber of peers that have not joined the dht: '+`self.notJoined`+'\n\n')
		f = open(self.filename, 'w')
		for node in self.nAlive:
			print >>f,node
		f.close()
		self.f2.close()
		reactor.stop()
		

class KClientBestNodes(KClientStatusCheck):
	def __init__(self,filename, filename2, n):
		KClientStatusCheck.__init__(self,filename, filename2)
		self.actives = 0
		self.notJoined = 0
		self.nnodes = 0
		self.n = n
		self.times = {}
		
	def getNodes(self):
		"""Select all dht nodes from file"""
		KClientOT.getNodes(self)
		self.nnodes = len(self.nodes)
	
	def connectionMade(self, node):
		self.connecting[node].reset(TOUT)
		msg = {'mid':1,'cmd':GS,'value':'Give me your stats'}
		self.times[node] = self.connections[node].sendRequest(msg)
		
	def gotReply(self,rep):
		self.actives = self.actives + 1
		if rep[1]['cmd'] == OK:
			log.msg("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
			self.times[rep[0]] = time.time() - self.times[rep[0]]
		else:
			log.err("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		if not rep[1]['value']['joined']:
			self.notJoined = self.notJoined + 1
		else:
			self.nAlive.append(rep[0])
		self.port[rep[0]].disconnect()
		self.connections[rep[0]].stopFactory()
		del self.connections[rep[0]]
		if not self.completed:
			self.completed = self.startConnecting()
		
	def complete(self):
		if DEBUG: print 'Just waited some time to let nodes to reply'
		if len(self.connections):
			reactor.callLater(SHUTDOWNTIME,self.complete)
			return
		log.msg('Number of active nodes: '+`self.actives`+'\nNumber of peers that have not joined the dht: '+`self.notJoined`+'\n\n')
		i = 0
		for key, value in sorted(self.times.iteritems(), key=lambda (k,v): (v,k)):
			if i < self.n:
				print >>self.f2,key
			i += 1
		self.f2.close()
		f = open(self.filename, 'w')
		for node in self.nAlive:
			print >>f,node
		f.close()
		reactor.stop()



################################################################################################
#Not ready for use##############################################################################
################################################################################################
class KClientStats(KClientOT):
	"""A client that contact every node that is up and request his status, logging it in a log file"""
	
	def __init__(self,filename):
		self.filename = filename
		self.connections = {}
		self.port = {}
		self.connecting = {}
		self.sent = {}
	
	def connectionMade(self, node):
		self.connecting[node].reset(TOUT)
		msg = {'mid':1,'cmd':GSTS,'value':'Give me your stats'}
		self.connections[node].sendRequest(msg)
		
	def gotReply(self,rep):
		if rep[1]['cmd'] == OK:
			log.msg("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		else:
			log.err("Got reply: "+`rep[1]`+"from node: "+`rep[0]`)
		f = open('tmp/'+rep[0]+'.html','w')
		print >>f, "<h1>"+rep[0]+"</h1>"+rep[1]['value']
		f.close()
		self.port[rep[0]].disconnect()
		self.connections[rep[0]].stopFactory()
		del self.connections[rep[0]]
		self.startConnecting()
