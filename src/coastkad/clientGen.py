from coastkad import rgen

NTIME = config.getfloat('KCLIENT','NTIME')
NEPAR = config.getfloat('KCLIENT','NEPAR')
SELNEXT = config.getint('KCLIENT','SELNEXT')
NEXTOP = config.getint('KCLIENT','NEXTOP')

class ClientGen:
	def __init__(self):
		self.time = None
		self.operations = None 
		self.values = None
		self.nop = 1
		if NTIME == 0:
			self.time = rgen.ExpNeg(NEPAR)
		else:
			self.time = rgen.Const(NTIME)
		if SELNEXT == 1:
			self.operations = rgen.ArrayComposition(([0.5,0.5],[1,2]))
		else:
			self.operations = rgen.Const(NEXOP)
		self.values = DbReaderZipf(n =5000, alpha =1)
	
	def getNextTime(self):
		return self.time.random()
		
	def getNextOp(self):
		self.nop = self.operations.random()
		return self.nop
		
	def getNextVal(self):
		ret = self.values.getNext
		return ret
		
