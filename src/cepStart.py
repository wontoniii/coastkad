"""Author: Francesco Bronzino
Description: Module used to start an instance of a CEP node
"""

#!/usr/bin/env python

from twisted.internet import reactor
from twisted.internet.abstract import isIPAddress
from coastkad.kconf import config as kConf
from coastkad.kDht import KDHT
from cep.conf import config as cepConf
from cep.filesystem import Filesystem
from cep.cepCache import CepCache
from cep.webClient import CepWebClient
from cep.cepCcl import Ccl
from cep.cepCore import CepCore
from cep.rgen import Const, ExpNeg
from cep.contentManager import DBReader
from cep.utils import *
from cep.clientGen import ClientGenerator
from cep.cepInterface import CepInterface
from cep.alto import AltoClient
from cep.dhtInterface import DhtInterface
from cep.statsLogger import StatsLogger
from cep.webServer import CepHTTPServer
from cep.db import DB
from cep.webServer import CepHandler
import os
import random
import time
import socket
import sys
import threading

DEBUG = True
MAXTIME = kConf.getint('KSERVER','MAXTIME')
INITTIME = kConf.getint('KSERVER','INITTIME')
WCPATH = cepConf.getstring('WEB_CLIENT', 'WCPATH')
WSPATH = cepConf.getstring('WEB_SERVER', 'WSPATH')
WSPORT = cepConf.getint('WEB_SERVER', 'WSPORT')
CEPCACHE = cepConf.getstring('DEFAULT', 'CEPCACHE')
USEF = cepConf.getboolean('DEFAULT', 'USEF')
DBFILE = cepConf.getstring('DEFAULT', 'DBFILE')
CACHESIZE = cepConf.getint('DEFAULT', 'CACHESIZE')
CACHEPOLICY = cepConf.getint('DEFAULT', 'CACHEPOLICY')
CDN_US_EAST = cepConf.getstringlist('DEFAULT', 'CDN_US_EAST')
CDN_US_WEST = cepConf.getstringlist('DEFAULT', 'CDN_US_WEST')
CDN_EU = cepConf.getstringlist('DEFAULT', 'CDN_EU')
CDN_AS = cepConf.getstringlist('DEFAULT', 'CDN_AS')
CDNPORT = cepConf.getint('DEFAULT', 'CDNPORT')
CDISTR = cepConf.getint('CLIENT_SIM', 'CDISTR')
CPAR = cepConf.getfloat('CLIENT_SIM', 'CPAR')
CSLOPE = cepConf.getint('CLIENT_SIM', 'CSLOPE')
CKEYS = cepConf.getstring('CLIENT_SIM', 'CKEYS')
LOGGER = cepConf.getstring('DEFAULT', 'LOGGER')
P2PCONC = cepConf.getint('DEFAULT', 'P2PCONC')
MINP2P = cepConf.getint('DEFAULT', 'MINP2P') 
MAXCONC = cepConf.getint('DEFAULT', 'MAXCONC')
P2PTH = cepConf.getint('DEFAULT', 'P2PTH')
ACTP2P = cepConf.getboolean('DEFAULT', 'ACTP2P')
ALTOCOORDS = cepConf.getstring('DEFAULT', 'ALTOCOORDS')
WSFILE = cepConf.getstring('WEB_SERVER', 'WSFILE')
NREQ = cepConf.getint('CLIENT_SIM', 'NREQ')
INITWAIT = cepConf.getint('CLIENT_SIM', 'INITWAIT')
DISTCDN = cepConf.getint('DEFAULT', 'DISTCDN')
CSIZE = cepConf.getint('DEFAULT', 'CSIZE')
CSEED = cepConf.getint('CLIENT_SIM', 'CSEED')
RSEED = cepConf.getint('CLIENT_SIM', 'RSEED')
P2PQ = cepConf.getfloat('DEFAULT', 'P2PQ')
MAXP2P = cepConf.getint('DEFAULT', 'MAXP2P')
REFT = cepConf.getint('DEFAULT', 'REFT')
TOUT = cepConf.getint('DEFAULT', 'TOUT')
BUFSIZE = cepConf.getint('DEFAULT', 'BUFSIZE')

class CepBuilder:
	"""Class used to instantiate a CEP object"""
	
	def __init__(self):
		"""The parameters used are taken from the configuration file"""
		self.CSEED = CSEED
		self.RSEED = RSEED	
		if CSEED < 0:
			self.CSEED = None
		if RSEED < 0:
			self.RSEED = None
		self.logger = StatsLogger(LOGGER)
		self.dht = KDHT()
		self.dht.loadConfig(kConf, kConf.get('DEFAULT', 'DHT'))
		self.idht = DhtInterface(dht= self.dht)
		self.t = INITTIME
		self.address = None
		reactor.resolve(socket.gethostname()).addCallbacks(self._gotOwnIP,self._resolveOwnFailed)
		self.filesystem = None
		if USEF:
			self.filesystem = Filesystem(CEPCACHE)
		else:
			self.filesystem = Filesystem(None)
		self.cache = CepCache(DBFILE, CACHESIZE, CACHEPOLICY, self.filesystem)
		self.clientHTTP = CepWebClient(WCPATH)
		self.serverHTTP = CepHTTPServer(('', WSPORT), CepHandler)
		self.serverHTTP.setup(WSPATH, DBFILE, self.logger, filename = WSFILE)
		# Start a thread with the server -- that thread will then start one
		# more thread for each request
		server_thread = threading.Thread(target=self.serverHTTP.serve_forever)
		# Exit the server thread when the main thread terminates
		server_thread.daemon = True
		server_thread.start()
		self.failed = 0
		self.cdn = []
		#Su alto c'e' ancora da lavorarci un secondo
		self.altoClient = AltoClient(ALTOCOORDS)
		self.arrivals = None
		if CDISTR == CONST:
			self.arrivals = Const(CPAR, x = self.CSEED)
		elif CDISTR == EXP:
			self.arrivals = ExpNeg(CPAR, x = self.CSEED)
		self.contents = DBReader(CKEYS, CSLOPE, seed = self.RSEED)
		self.ips = False
		self.joined = False
		
	def _gotOwnIP(self, host):
		"""Store the IP address of the node.
		
		Arguments:
		host -- the IP address stored as a string
		"""
		if DEBUG: print 'CepBuilder: Own addr:',host
		self.address = [host, WSPORT]
		
	def _resolveFailed(self, host, port):
		"""Function called once a DNS lookup fails.
		
		Arguments:
		host -- should be empty
		port -- should be empty
		"""
		if DEBUG: print 'CepBuilder: file to discover own address:'
		
	def _resolveOwnFailed(self, host):
		"""Function called once a DNS lookup for the self address fails.
		
		Arguments:
		host -- should be empty
		port -- should be empty
		"""
		if DEBUG: print 'CepBuilder: file to discover own address:'
		
	def _gotIP(self, host,port):
		"""Store the IP address of a CDN node
		
		Arguments:
		host -- IP address of the node as string
		port -- port used as an integer
		"""
		if DEBUG: print 'CepBuilder: CDN addr:',host,port
		self.cdn.append((host,port))
		if len(self.cdn) + self.failed == len(CDNADDRS):
			reactor.callLater(0,self.contactAlto)
			
	def contactAlto(self):
		"""Ask the ALTO client to contact the server to order the list of addresses.
		This function is used to find the closest CDN node.
		"""
		#Indirizzi fittizzi usati per indicare le localita' geografiche al server ALTO
		addrs = ['1.1.1.1','2.2.2.2','3.3.3.3','4.4.4.4']
		df = self.altoClient.analyze('1', self.address[0], addrs)
		df.addCallback(self.gotFromAlto)
		
	def gotFromAlto(self, (key,addrs)):
		"""Receive the response of the ALTO query done to discover the closest CDN node.
		
		Arguments:
		key -- always '1'
		addrs -- the list of ordered CDN addresses
		"""
		if addrs[0][0] == '1.1.1.1':
			temp = CDN_US_EAST
		elif addrs[0][0] == '2.2.2.2':
			temp = CDN_US_WEST
		elif addrs[0][0] == '3.3.3.3':
			temp = CDN_EU
		elif addrs[0][0] == '4.4.4.4':
			temp = CDN_AS
		else:
			if DEBUG: print "ERROR: error finding the closest cdn group"
			return
		for node in temp:
			self.cdn.append(((node,CDNPORT),(addrs[0][1]+1)*DISTCDN))
		if DEBUG:
			for node in self.cdn:
				print "CepBuilder: CDN node:",`node`
		self.ips = True
		reactor.callLater(0,self._finishStart)
			
	def _finishStart(self):
		"""Wait for the all the instantiation processes to finish and then start the CEP node job."""
		if not self.joined or not self.ips:
			if DEBUG: print 'CepBuilder: still not complete'
			return
		self.ccl = Ccl(self.address, self.cdn, self.altoClient, dht = self.idht, MINP2P = MINP2P, MAXCONC = MAXCONC, P2PTH = P2PTH, ACTP2P = ACTP2P, CSIZE = CSIZE, P2PQ = P2PQ, MAXP2P = MAXP2P)
		self.ccore = CepCore(self.cache, self.clientHTTP, self.ccl, self.address, self.idht, self.logger, P2PCONC = P2PCONC, REFT = REFT, TOUT = TOUT, BUFSIZE = BUFSIZE, ACTP2P = ACTP2P)
		self.iCep = CepInterface(self.ccore, self.logger, BUFSIZE = BUFSIZE)
		self.clients = ClientGenerator(self.iCep, self.arrivals, self.contents, self.logger, mreq = NREQ)
		self.logger.startLogging(time.time())
		self.clients.start(INITWAIT)
		if DEBUG: print 'CepBuilder: Everything ready'
		
	def getCdn(self):
		"""Wait for the own address and then contact ALTO to find the closest CDN node."""
		if not self.address:
			reactor.callLater(1,self.getCdn)
			return
		if DEBUG: print "Got my own address: ", self.address
		self.contactAlto()
		
	def _nextTime(self):
		"""Next wait time for trying to connect to the DHT."""
		ret = random.randint(0,self.t)
		if self.t < MAXTIME:
			self.t*2
		return ret
		
	def _joined(self,result):
		"""The node has succesfully joined the network.
		
		Arguments:
		result -- address used to join the DHT
		"""
		msg = 'Joined the DHT with this address: '+`result`
		if DEBUG: print msg
		#log.msg(msg)
		self.joined = True
		reactor.callLater(0,self._finishStart)
		reactor.callLater(60*60*3,self.checkJoined)
	
	def _joinError(self,reason):
		"""The node has succesfully joined the network.
		
		Arguments:
		reason -- the failure reason"""
		msg = 'ERROR: Not joined for this reason: '+`reason`
		log.err(msg)
		reactor.callFromThread(self.join)
		
	def join(self):
		"""The node has succesfully joined the network."""
		time.sleep(self._nextTime())
		#log.msg('Start joining the DHT')
		self.df = self.dht.join()
		self.df.addCallbacks(self._joined, self._joinError)
		
	def checkJoined(self):
		"""Check if the node joined the DHT."""
		if not self.dht.getJoined():
			reactor.callFromThread(self.join)
		else:
			reactor.callLater(60*60*3, self.checkJoined)
		
	def start(self):
		"""Start the CEP node job."""
		reactor.callLater(0,self.getCdn)
		reactor.callLater(0,self.join)
		reactor.run()
		self.logger.finishLogging(time.time())
		#self.serverHTTP.shutdown()
		
	def stop(self):
		"""Stop the CEP node job."""
		self.logger.finishLogging(time.time())
		reactor.stop()
		if DEBUG: print 'CepBuilder: finished everything'
		
if __name__=='__main__':
	d = CepBuilder()
	d.start()
